=pod

=head1 NAME

RefTree

=head1 DESCRIPTION

The main class for RefTree, which inherits the other RefTree::* classes (i.e. don't use them directly, just use this package).

=head1 SEE ALSO

C<RefTree::Taxonomy>, C<RefTree::Attributes>, C<RefTree::Databases>

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2015 by the United States Department of Energy Joint Genome Institute

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

package RefTree;

# Object design: inheritance was chosen over composition because:
# - the classes are extending the functionality of the base class
# - classes require access to the lower classes' data and methods
# - the classes won't be reused in any other code (too specific to RefTree and the traversals supplied by the base class)
# - inheritance made the code more concise and readable
# - without inheritance, several of the classes would have required back-references to the base class which wastes RAM (there are many node objects) and made the code ugly

# Intended audience:
# - RefTree was originally written by Edward Kirton as part of the reftreeSearch.pl tool
# - it was later adapted to allow Adam Rivers to use it in his Gene Pattern Classifier tool
# - it's really internal software that was tailored to specific use cases; while it's general utility is suggested, RefTree will evolve according to the use cases we adopt
# - the author is open to accepting code changes/feature additions from other developers; fork me on bitbucket

use parent qw(RefTree::Databases); # or whatever the last class in the inheritance chain is -- that is, this may change in the future, but RefTree will always be the package you use -- don't use other classes directly.

1;

__END__
