=pod

=head1 NAME

RefTree::Taxonomy

=head1 DESCRIPTION

Class description for a (tree of) taxonomic node(s) in the RefTree.  It may be an internal node or a leaf.  It always has an ID, Rank, Name, and exactly one Parent (for the root, it's parent is itself); a node may have one or more children.  Importantly, this class provides generalized tree traversal methods.

=head2 TAXONOMY

The NCBI taxonomy has the following characteristics:

=over 5

=item *

All lineages are guaranteed to terminate at the root

=item *

Not all lineages have all major ranks defined

=item *

Classification and the existence of nodes are subject to change between releases

=item *

In rare cases, a node may have a parent of the same rank.

=item *

Nodes of rank "no rank" may appear anywhere in the taxonomic lineage.

=item *

Refseq, blast db, and taxdump aren't necessarily consistent or complete.  Sequences for taxa not in taxdump are discarded.

=item *

RefTree has introduced rank IDs (integers).  While rank names (e.g. "phylum") are still available, the rank IDs are comparable with numerical comparison operators, where the ID increases with increased levels of classification (i.e. species > genus).  Some nodes are classified as "no rank", which has an ID of undef (and cannot readily be compared).

=back

=head1 METHODS

=over 5

=cut

package RefTree::Taxonomy;

use strict;
use Carp;
use Env qw(REFTREE_DIR TMPDIR);
use File::Path qw(make_path remove_tree);
use constant
{
	FALSE => 0, TRUE => 1, # for readability
	# NODE OBJECT IS FIXED-LENGTH ARRAY AND THESE ARE THE INDICIES
	TAXON => 0,
	RANK => 1,
	PARENT => 2,
	CHILDREN => 3
};
use DBI;
use File::Slurp;
use Contextual::Return;

# GLOBAL VARIABLES (TREE IS SINGLETON)
our %nodes; # THERE IS ONLY ONE TREE (SINGLETON)
our $root;

# TAXONOMIC CLASSIFICATION
our @ranks =
(
	'superkingdom',
	'kingdom',
	'subkingdom',
	'superphylum',
	'phylum',
	'subphylum',
	'superclass',
	'class',
	'subclass',
	'infraclass',
	'superorder',
	'order',
	'suborder',
	'infraorder',
	'parvorder',
	'superfamily',
	'family',
	'subfamily',
	'tribe',
	'subtribe',
	'genus',
	'subgenus',
	'species group',
	'species subgroup',
	'species',
	'subspecies',
	'varietas',
	'forma'
); # ordered list of taxonomic ranks; additionally, an index of undef corresponds to 'no rank'
our %rankLevel =
(
	'no rank' => undef,
	superkingdom => 0,
	kingdom => 1,
	subkingdom => 2,
	superphylum => 3,
	phylum => 4,
	subphylum => 5,
	superclass => 6,
	class => 7,
	subclass => 8,
	infraclass => 9,
	superorder => 10,
	order => 11,
	suborder => 12,
	infraorder => 13,
	parvorder => 14,
	superfamily => 15,
	family => 16,
	subfamily => 17,
	tribe => 18,
	subtribe => 19,
	genus => 20,
	subgenus => 21,
	'species group' => 22,
	'species subgroup' => 23,
	species => 24,
	subspecies => 25,
	varietas => 26,
	forma => 27
); # convert rank name (string) into RefTree rank ID (integer); note that a rank ID of undef corresponds to 'no rank' and such nodes appear at various levels in the tree.
our %majorRanks =
(
	superkingdom => 0, 2  => 0,
	phylum => 1, 4  => 1,
	class => 2, 7  => 2,
	order => 3, 11 => 3,
	family => 4, 16 => 4,
	genus => 5, 20 => 5,
	species => 6, 24 => 6
); # rank ID or name => index on node's @classification

# REFTREE DATA
our $refTreeDmpFile = "$REFTREE_DIR/REFTREE.dmp";
our $refTreeDbFile = "$REFTREE_DIR/REFTREE.db";
our $refTreeVers; # md5sum will be set when tree loaded/created
our $dbh; # refTree.db database handle
our $sthSelectNode; # cached statement handle for selecting node (for lazy initialization)
our $sthSelectChildren; # cached statement handle for selecting children (for lazy initialization)
our $lazy = TRUE; # TRUE when using lazy loading
our $refTreeVersion;
our $dbh;
our $sthSelectNode;
our $maxFileId;

=head2 Node Initialization

=over 5

=item newNode

Initialize a node and save in global %nodes variable (taxonomic nodes are singletons).  Nodes are stored as arrays because they are smaller and faster than hashes (and there are a lot of nodes).

=cut

sub newNode
{
	my ($class, $taxon, $rankId, $parent) = @_;
	$taxon or confess("Taxon ID required\n");
	$taxon =~ /^\d+$/ or confess("Invalid taxon ID: $taxon\n");
	# note that the @children arrayref is undefined -- we use this to distinguish between a node with no children and a lazy node that hasn't had it's children initialized yet
	my $this = [ $taxon, $rankId, $parent, undef ];
	bless $this, $class;
	defined($this->[PARENT]) or $root = $this;
	return $nodes{$this->id} = $this;
}

=item loadTree

Load and initialize all nodes in tree from reftree.dmp file and return root.  The dmp file records are ordered such that the parent always proceeds it's children (thus the root is the first entry).  Returns root.

=cut

sub loadTree
{
	my $class = shift;
	defined($REFTREE_DIR) or confess("\$REFTREE_DIR not defined\n");
	-d $REFTREE_DIR or confess("\$REFTREE_DIR does not exist: $REFTREE_DIR\n");
	-f $refTreeDmpFile or confess("refTree.dmp file not found\n");
	open(my $in, '<', $refTreeDmpFile) or confess("Unable to open $refTreeDmpFile: $!");
	# GET ROOT
	my $line = <$in>;
	chomp $line;
	my ($taxon, $parentId, $rankId) = split(/\t/, $line);
	$parentId or confess("Node parent taxon ID required\n");
	defined($rankId) or confess("Node rank required\n");
	$rankId eq '' and $rankId = undef;
	$parentId != $taxon and confess("Invalid REFTREE.dmp file\n");
	my $node = $class->newNode($taxon, $rankId, undef);
	my $stack = [ $node ];
	$node->log("Loading RefTree");
	$root->lazy(FALSE);
	# GET REMAINING NODES
	my $parent;
	while (<$in>)
	{
		chomp;
		($taxon, $parentId, $rankId) = split(/\t/);
		$parentId or confess("Node parent taxon ID required\n");
		defined($rankId) or confess("Node rank required\n");
		$rankId eq '' and $rankId = undef;
		pop @$stack while ( @$stack and $stack->[$#$stack]->id != $parentId );
		$parentId != $stack->[$#$stack]->id and confess("Invalid REFTREE.dmp file; parent not on stack\n");
		$parent = $stack->[$#$stack];
		$node = $class->newNode($taxon, $rankId, $parent);
		push @{$parent->[CHILDREN]}, $node;
		push @$stack, $node;
	}
	close($in);
	return $root; # root was set by newNode
}

=item loadNode

Lazy initialization of a node and all nodes in it's taxonomic path to root.  Only parent links are initialized; children links are initialized as needed.  This method is appropriate when accessing only a relatively small number of nodes (e.g. <10%).  Returns the specified node (unlike the other constructors which return the root).  The optional parent ref is provided by &children method when initializing child nodes and links.

=cut

sub loadNode
{
	my ($class, $taxon, $parent) = @_;
	exists($nodes{$taxon}) and return $nodes{$taxon};
	defined($dbh) or $dbh = DBI->connect("DBI:SQLite:$refTreeDbFile") or confess("Unable to read SQLiteDb, $refTreeDbFile: ".DBI->errstr."\n");
	defined($sthSelectNode) or $sthSelectNode = $dbh->prepare_cached('SELECT parent, rank FROM refTree where taxon = ?') or confess("Couldn't prepare statement: ".$dbh->errstr);
	$sthSelectNode->execute($taxon);
	my @row = $sthSelectNode->fetchrow_array;
	@row or confess("Unknown taxon: $taxon\n");
	my ($parentId, $rankId) = @row;
	$rankId eq '' and $rankId = undef;
	if ( defined($parent) and $parent->id == $parentId ) { 1 } # parent was provided
	elsif ( $parentId == $taxon ) { $parent = undef } # root
	else { $parent = $class->loadNode($parentId) } # recursive to root
	my $node = $class->newNode($taxon, $rankId, $parent);
	!defined($parent) and $root->lazy(TRUE);
	return $node;
}

=item createTree

Initialize all nodes from an NCBI nodes.dmp file and write REFTREE.dmp and .db files.  Returns root.

=cut

sub createTree
{

	my ($class, $taxdumpDir) = @_;

	# VALIDATE
	defined($REFTREE_DIR) or confess("\$REFTREE_DIR not defined\n");
	$taxdumpDir or confess("Missing required taxdump dir\n");
	$taxdumpDir = File::Spec->rel2abs($taxdumpDir);
	-d $taxdumpDir or confess("Invalid taxdump dir: $taxdumpDir\n");

	# INITIALIZE NODES -- TEMPORARILY WITHOUT LINKS (until later below)
	my $node;
	my $in;
	my $file = "$taxdumpDir/nodes.dmp";
	if ( -f "$file.gz" )
	{
		open($in, "gunzip -c $file.gz |") or confess("Unable to read gzipped file, $file.gz: $!");
	} else
	{
		open($in, '<', $file) or confess("Unable to read file, $file: $!");
	}
	while (<$in>)
	{
		my ($taxon, $parentId, $rankName, $emblCode, $divisionId, $inherited, $code) = split(/\t\|\t/);
		$taxon =~ /^\d+$/ or confess("Invalid taxon ID: $taxon\n");
		$parentId =~ /^\d+$/ or confess("Invalid parent taxon ID: $parentId\n");
		exists($rankLevel{$rankName}) or confess("Invalid rank: $rankName\n");
		my $rankId = $rankLevel{$rankName};
		$node = $class->newNode($taxon, $rankId, $parentId); # temporarily using parentId instead of parent object ref (until changed below)
		$. == 1 and $node->log("Reading nodes.dmp");
	}
	close($in);

	# INITIALIZE LINKS -- WHEN NODES ARE CREATED (ABOVE), THE PARENT MAY NOT EXIST YET, SO ITS ID IS TEMPORARILY STORED AND MUST BE REPLACED BY THE OBJECT REF HERE.  IN ADDITION, THE CHILDREN LINKS MUST BEEN CREATED.
	$node->log("Initializing links");
	my $parent;
	foreach $node ( values %nodes )
	{
		my $parentId = $node->parent;
		if ( $node->id == $parentId )
		{
			# ROOT HAS UNDEF REF
			$node->[PARENT] = undef;
			$root = $node; # was not set by newNode
			$root->lazy(FALSE);
		} else
		{
			# REPLACE PARENT ID WITH PARENT REF
			my $parent = $node->node($parentId) or confess("Unknown taxon, parent $parentId\n");
			$node->[PARENT] = $parent;
			# CREATE CHILD LINK
			push @{$parent->[CHILDREN]}, $node;
		}
	}

	# SAVE REFTREE FILES
	$node->log("Writing refTree.dmp file");
	-d $REFTREE_DIR or make_path($REFTREE_DIR) or confess("Unable to mkdir $REFTREE_DIR: $!");

	# CREATE SQLITE3 DATABASE
	$dbh = DBI->connect("DBI:SQLite:$refTreeDbFile", { RaiseError => 1 }) or confess(DBI->errstr);
	$dbh->{AutoCommit} = 0;
	$dbh->do('CREATE TABLE reftree ( taxon INTEGER PRIMARY KEY, parent INTEGER NOT NULL, rank INTEGER)');
	my $sth = $dbh->prepare_cached('INSERT INTO refTree ( taxon, parent, rank ) VALUES ( ?, ?, ? )');

	# TRAVERSE
	my $out = IO::File->new(">$refTreeDmpFile") or confess("Unable to write to $refTreeDmpFile: $!");
	my $counter = 0;
	$root->descend(
	{
		preorder => sub
		{
			my ($this, $out, $sth, $dbh, $counterSR) = @_;
			my @row = ( $this->id, $this->parentId, $this->rankId // '' );
			print $out join("\t", @row),"\n";
			$sth->execute(@row);
			++$$counterSR % 10_000 or $dbh->commit;
		}
	}, $out, $sth, $dbh, \$counter);
	$out->close;
	$dbh->commit;
	$sth->finish;

	# DONE
	return $root;
}

=item lazy

TRUE when using lazy loading

=cut

sub lazy
{
	my ($this, $flag) = @_;
	defined($flag) and $lazy = $flag ? TRUE : FALSE;
	return $lazy;
}

=item version

Returns RefTree version string (md5sum of reftree.dmp file); calculate and set if undef.

=cut

sub version
{
	$refTreeVersion and return($refTreeVersion);
	my $output = `md5sum $refTreeDmpFile`;
	$output =~ /^(\w+)/ or confess("Unable to calculate md5sum for reftree.dmp: $!\n");
	return $refTreeVersion = $1;
}


=item node

Returns a node given a taxon ID or undef if unknown.  This provides random access to any taxon via it's ID.  Returns undef if taxon is unknown.

=cut

sub node
{
	my ($this, $taxon) = @_;
	return $nodes{$taxon};
}

=item nodes

Returns a reference to the %nodes hash which contains all initialized nodes.

=cut

sub nodes { return \%nodes }

=item root

Returns the root node object.

=cut

sub root { return $root }

=back

=head2 Accessors

=over 5

=item id

Returns the taxon id of the node

=cut

sub id { return shift->[TAXON] }

=item taxon

Returns the taxon id of the node.  Alias for &id.

=cut

sub taxon { return shift->[TAXON] }

=item parent

Returns the object (not the ID) of the parent.  The parent of the root is undef.  Even when using lazy loading, every loaded node's parent is guaranteed to be initialized as well.

=cut

sub parent  { return shift->[PARENT] }

=item parentId

Returns the taxon ID of the parent node.  The root returns it's own ID (not undef), which is the way NCBI Taxonomy does it.

=cut

sub parentId
{
	my $this = shift;
	return defined($this->[PARENT]) ? $this->parent->id : $this->id;
}

=item rankId

Returns the rank ID of the node (index on @ranks), where undef corresponds to 'no rank'.

=cut

sub rankId { return shift->[RANK] }

=item rankName

Returns the node's rank name (controlled vocabulary).

=cut

sub rankName
{
	my $this = shift;
	return defined($this->[RANK]) ? $ranks[$this->[RANK]] : 'no rank';
}

=item numChildren

Returns the number of children this node has.

=cut

sub numChildren
{
	my $this = shift;
	my @children = $this->children;
	return scalar(@children);
}

=item childrenIds

Returns a sorted list of the child node IDs (integers, not objects).

=cut

sub childrenIds { return map { $_->id } shift->children }

=item children

Returns a list of the child nodes (objects, not IDs), sorted by taxon ID.  When using lazy loading, children are not initialized until accessed.

=cut

sub children
{
	my $this = shift;
	if ( $this->lazy and ! defined($this->[CHILDREN]) )
	{
		# LAZY LOADING OF ALL CHILDREN OF THIS NODE
		defined($dbh) or $dbh = DBI->connect("DBI:SQLite:$refTreeDbFile") or confess("Unable to read SQLiteDb, $refTreeDbFile: ".DBI->errstr."\n");
		defined($sthSelectChildren) or $sthSelectChildren = $dbh->prepare_cached('SELECT taxon, rank FROM reftree where parent = ? order by taxon') or confess("Couldn't prepare statement: ".$dbh->errstr);
		$sthSelectChildren->execute($this->id);
		$this->[CHILDREN] = [];
		while ( my @row = $sthSelectChildren->fetchrow_array )
		{
			# INIT NODE
			my ($taxon, $rankId) = @row;
			my $class = ref($this);
			my $node = $class->newNode($taxon, $rankId, $this);
			push @{$this->[CHILDREN]}, $node;
		}
	}
	defined($this->[CHILDREN]) or $this->[CHILDREN] = [];
	return @{$this->[CHILDREN]};
}

=item field

Returns a field value(s) given the keyword/name or undef if unknown.  Even scalar values are returned in an arrayref.

=cut

sub field
{
	my ($this, $field) = @_;
	if ( $field eq 'id' ) { return $this->id }
	elsif ( $field eq 'taxon' ) { return $this->id }
	elsif ( $field eq 'rankId' ) { return $this->rankId }
	elsif ( $field eq 'rankName' ) { return $this->rankName }
	elsif ( $field eq 'parentId' ) { return $this->parentId }
	elsif ( $field eq 'taxonomy' ) { return scalar $this->taxonomy } # TODO ADD FORMATTING OPTIONS
	else { confess("Unknown field: $field\n") }
}

=item record

Returns list of requested information about a node as either a list or commana-delimited string, depending on the context.  If the fields are not specified, the default fields are: taxonId, rankId, parentId.

=cut

sub record
{
	my ($this, $fields) = @_;
	unless ( defined($fields) and @$fields > 0 ) { $fields = [ 'taxon', 'rankId', 'parentId' ] }
	my @result;
	my %result;
	foreach (@$fields)
	{
		push @result, $result{$_} = $this->field($_);
	}
	return
		SCALAR { join(',', @result) }
		LIST { @result }
		ARRAYREF { \@result }
		HASHREF { \%result };
}

=item output

Outputs the node's record to the file handle or standard output.

=cut

sub output
{
	my ($this, $fh, $fields) = @_;
	defined($fh) or $fh = *STDOUT;
	print $fh join("\t", $this->record($fields)), "\n";
	return 1;
}

=item outputSubtree

Output all nodes in the subtree.

=cut

sub outputSubtree
{
	my $this = shift;
	my $select = shift;
	return $this->descend(
	{
		preorder => sub { return shift->output(@_) },
		select => $select,
		result => 'sum'
	}, @_);
}

=back

=head2 Traversal Methods

=over 5

=item ascend

Traverse tree via parent links.  Action is preorder and by default unshifts onto result array.

=cut

sub ascend
{
	my ($this, $param, @args) = @_;

	# VERIFY NODES ARE LINKED
	if ( $this->parent )
	{
		ref($this->parent) or confess("Traversals not possible with unlinked nodes.\n");
	} else
	{
		$this->children or confess("Traversals not possible with unlinked nodes.\n");
	}

	# VALIDATE PARAMETERS
	defined($param) or confess("Traversal parameters required\n");
	ref($param) eq 'HASH' or confess("Invalid traversal parameters\n");
	if ( defined($param->{action}) )
	{
		ref($param->{action}) eq 'CODE' or confess("Action must be a coderef\n");
	}
	else
	{
		confess("A (preorder) action subref is required\n");
	}
	if ( defined($param->{min}) and defined($param->{max}) and $param->{min} > $param->{max} ) { confess("Min cannot be greater than max\n") }
	if ( defined($param->{pretest}) )
	{
		ref($param->{pretest}) eq 'CODE' or confess("Pretest must be a coderef\n");
	}
	elsif ( defined($param->{posttest}) )
	{
		ref($param->{posttest}) eq 'CODE' or confess("Posttest must be a coderef\n");
	}

	# TRAVERSE
	my $inRange = ( exists($param->{min}) or exists($param->{max}) ) ? FALSE : TRUE;
	return $this->_ascend($inRange, $param, @args);
}

=item _ascend

Private recursive method for ascending traversals.

=cut

sub _ascend
{
	my ($this, $inRange, $param, @args) = @_;

	# INIT VARS
	my @result = ();
	my $result = 0;

	# BOUNDED TRAVERSAL
	if ( defined($param->{min}) or defined($param->{max}) )
	{
		if ( defined($param->{min}) and defined($this->rankId) and $this->rankId < $param->{min} )
		{
			return $param->{result} eq 'sum' ? $result : @result;
		}
		if ( defined($param->{max}) and $this->rankId <= $param->{max} )
		{
			$inRange = TRUE;
		}
	}

	# ACTION
	if ( $inRange )
	{
		if ( $param->{result} eq 'sum' ) { $result += &{$param->{action}}($this, @args) }
		elsif ( $param->{result} eq 'push' ) { push @result, &{$param->{action}}($this, @args) }
		else { unshift @result, &{$param->{action}}($this, @args) }
	}

	# WALK
	if ( defined($this->parent) )
	{
		if ( $param->{result} eq 'sum' )
		{
			$result += $this->parent->_ascend($inRange, $param, @args);
		}
		elsif ( $param->{result} eq 'push' )
		{
			push @result, $this->parent->_ascend($inRange, $param, @args);
		}
		else
		{
			unshift @result, $this->parent->_ascend($inRange, $param, @args);
		}
	}

	return $param->{result} eq 'sum' ? $result : @result;
}

=item descend

Traverse tree with support for:

	* depth-first (default) vs breadth-first (level-order) traversal ('shallowing', 'deepening')
	* rank-bounded traversal ('min', 'max' params; using integer rank)
	* weighted traversal ('weight' param indicating attribute with numeric value to compare)
	* preorder, inorder, and postorder traversals.  In-order traversals require the weight parameter
	* the pretest sub is evaluated before any action or visiting children; it ends the traversal when it evaluates to TRUE
	* the posttest sub is evaluated after the action (thus would have no effect in post-order traversals)
	* actions either push return value onto @result list (by default), unshift onto @result, or sum the $result scalar

The action subs receive the node and optional @args list.  The test subs receive the node, \@results arrayref, and optional @args list.

=cut

sub descend
{
	my ($this, $param, @args) = @_;

	# VERIFY NODES ARE LINKED
	if ( $this->parent )
	{
		ref($this->parent) or confess("Traversals not possible with unlinked nodes.\n");
	} else
	{
		$this->children or confess("Traversals not possible with unlinked nodes.\n");
	}

	# VALIDATE PARAMETERS
	defined($param) or confess("Traversal parameters required\n");
	ref($param) eq 'HASH' or confess("Invalid traversal parameters\n");
	if ( defined($param->{preorder}) )
	{
		ref($param->{preorder}) eq 'CODE' or confess("Preorder action must be a coderef\n");
	}
	elsif ( defined($param->{postorder}) )
	{
		ref($param->{postorder}) eq 'CODE' or confess("Postorder action must be a coderef\n");
	}
	elsif ( defined($param->{inorder}) )
	{
		
		ref($param->{inorder}) eq 'CODE' or confess("In-order action must be a coderef\n");
		$param->{weight} or confess("In-order traversal requires a weight attribute\n"); # ECCE
	} else
	{
		confess("Either a preorder, inorder, or postorder action subref is required\n");
	}
	if ( defined($param->{min}) and defined($param->{max}) and $param->{min} > $param->{max} ) { confess("Min cannot be greater than max\n") }
	if ( defined($param->{pretest}) )
	{
		ref($param->{pretest}) eq 'CODE' or confess("Pretest must be a coderef\n");
	}
	elsif ( defined($param->{posttest}) )
	{
		ref($param->{posttest}) eq 'CODE' or confess("Posttest must be a coderef\n");
		if ( $param->{postorder} ) { confess("A post-action test and post-order traversal is invalid\n") }
	}

	# TRAVERSAL METHODS
	if ( $param->{shallowing} )
	{
		# ITERATIVE SHALLOWING (LEVEL-ORDER/BREADTH-FIRST) TRAVERSAL
		my @result;
		my $min = $param->{min} // 0;
		my $max = $param->{max} // scalar(@ranks);
		for ( my $level = $max; $level >= $min; --$level )
		{
			$param->{min} = $param->{max} = $level;
			push @result, $this->_descend(FALSE, $param, @args);
		}
		$param->{min} = $min;
		$param->{max} = $max;
		return @result;
	}
	elsif ( $param->{deepening} )
	{
		# ITERATIVE DEEPENING (LEVEL-ORDER/BREADTH-FIRST) TRAVERSAL
		my @result;
		my $min = $param->{min} // 0;
		my $max = $param->{max} // scalar(@ranks);
		for ( my $level = $min; $level <= $max; ++$level )
		{
			$param->{min} = $param->{max} = $level;
			push @result, $this->_descend(FALSE, $param, @args);
		}
		$param->{min} = $min;
		$param->{max} = $max;
		return @result;
	} else
	{
		# DEPTH-FIRST TRAVERSAL
		my $inRange = ( exists($param->{min}) or exists($param->{max}) ) ? FALSE : TRUE;
		return $this->_descend($inRange, $param, @args);
	}
}

=item _descend

Depth-first traversal, applying the specified action on each node.  If min and max parameters are defined, then only nodes within the rank range will be acted upon.

=cut

sub _descend
{
	my ($this, $inRange, $param, @args) = @_;

	# INIT VARS
	my @result = ();
	my $result = 0;

	# BOUNDED TRAVERSAL
	if ( defined($param->{min}) or defined($param->{max}) )
	{
		if ( defined($param->{max}) and defined($this->rankId) and $this->rankId > $param->{max} )
		{
			return $param->{result} eq 'sum' ? $result : @result;
		}
		if ( defined($param->{min}) and $this->rankId >= $param->{min} )
		{
			$inRange = TRUE;
		}
	}

	# PRE-ACTION TEST FOR COMPLETION
	if ( $param->{pretest} and &{$param->{pretest}}($this, \@result, @args) )
	{
		return $param->{result} eq 'sum' ? $result : @result;
	}

	# PRE-ORDER ACTION
	if ( $param->{preorder} )
	{
		if ( $inRange and ( !$param->{select} or &{$param->{select}}($this) ) )
		{
			if ( $param->{result} eq 'sum' )
			{
				$result += &{$param->{preorder}}($this, @args);
			}
			elsif ( $param->{result} eq 'unshift' )
			{
				unshift @result, &{$param->{preorder}}($this, @args);
			}
			else
			{
				push @result, &{$param->{preorder}}($this, @args);
			}

			# POST-ACTION TEST FOR COMPLETION
			if ( $param->{posttest} and &{$param->{posttest}}($this, \@result, @args) )
			{
				return $param->{result} eq 'sum' ? $result : @result;
			}
		}
	}

	# WALK
	if ( $param->{weight} )
	{
		# WEIGHT-GUIDED TRAVERSAL
		# ECCE -- CHANGE WEIGHTING FROM PARAM TO A SUB (PARAMS DON'T EXIST IN THIS CLASS)
		# descends even when max rank because a node may (rarely) have a child of the same rank
		my @children;
		if ( $param->{inorder} )
		{
			@children = sort { $b->get($param->{weight}) <=> $a->get($param->{weight}) } $this->children;
			my $weight = $this->get($param->{weight});

			# VISIT CHILDREN WITH WEIGHT <= THIS NODE
			foreach my $child ( @children )
			{
				if ( $child->get($param->{weight}) <= $weight )
				{
					if ( $param->{result} eq 'sum' )
					{
						$result += $child->_descend($inRange, $param, @args);
					} elsif ( $param->{result} eq 'unshift' )
					{
						unshift @result, $child->_descend($inRange, $param, @args);
					} else
					{
						push @result, $child->_descend($inRange, $param, @args);
					}
				}
				else
				{ last }
			}

			# IN-ORDER ACTION
			if ( $inRange and ( !$param->{select} or &{$param->{select}}($this) ) )
			{
				if ( $param->{result} eq 'sum' )
				{
					$result += &{$param->{postorder}}($this, @args);
				}
				elsif ( $param->{result} eq 'unshift' )
				{
					unshift @result, &{$param->{postorder}}($this, @args);
				}
				else
				{
					push @result, &{$param->{postorder}}($this, @args);
				}

				# POST-ACTION TEST FOR COMPLETION
				if ( $param->{posttest} and &{$param->{posttest}}($this, \@result, @args) )
				{
					return $param->{result} eq 'sum' ? $result : @result;
				}
			}

			# VISIT CHILDREN WITH WEIGHT > THIS NODE
			foreach my $child ( @children )
			{
				if ( $child->get($param->{weight}) > $weight )
				{
					if ( $param->{result} eq 'sum' )
					{
						$result += $child->_descend($inRange, $param, @args);
					}
					elsif ( $param->{result} eq 'unshift' )
					{
						unshift @result, $child->_descend($inRange, $param, @args);
					} else
					{
						push @result, $child->_descend($inRange, $param, @args);
					}
				}
			}
		} else
		{
			@children = sort { $b->get($param->{weight}) <=> $a->get($param->{weight}) } $this->children;
			foreach my $child ( @children )
			{
				if ( $param->{result} eq 'sum' )
				{
					$result += $child->_descend($inRange, $param, @args);
				} elsif ( $param->{result} eq 'unshift' )
				{
					unshift @result, $child->_descend($inRange, $param, @args);
				} else
				{
					push @result, $child->_descend($inRange, $param, @args);
				}
			}
		}
	} else
	{
		if ( $this->numChildren )
		{
			#foreach my $child ( $this->children )
			my @children = $this->children;
			foreach my $child ( @children )
			{
				if ( $param->{result} eq 'sum' )
				{
					$result += $child->_descend($inRange, $param, @args);
				} elsif ( $param->{result} eq 'unshift' )
				{
					unshift @result, $child->_descend($inRange, $param, @args);
				} else
				{
					push @result, $child->_descend($inRange, $param, @args);
				}
			}
		}
	}
	
	# POST-ORDER ACTION
	if ( $param->{postorder} )
	{
		if ( $inRange and ( !$param->{select} or &{$param->{select}}($this) ) )
		{
			if ( $param->{result} eq 'sum' )
			{
				$result += &{$param->{postorder}}($this, @args);
			}
			elsif ( $param->{result} eq 'unshift' )
			{
				unshift @result, &{$param->{postorder}}($this, @args);
			}
			else
			{
				push @result, &{$param->{postorder}}($this, @args);
			}
		}
	}
	return $param->{result} eq 'sum' ? $result : @result;
}

=back

=head2 Taxonomic Classification Methods

=over 5

=item taxonomy

Returns the taxonomic path of this node.  In list context, returns an array of the node objects; in scalar context, returns a "/"-delimited string of the nodes.  The format of each node may be specified by the optional fields and delimiter parameters.  Fields indicates the metadata about each node to be included (default=taxon ID, rank ID); the delimiter is used to separate these fields (default=":").

=cut

sub taxonomy
{
	my ($this, $ranks, $fields, $delimiter) = @_;

	# GET OBJ LIST
	my @taxonomy = $this->ascend( { action => sub { return shift } } );
	wantarray and return @taxonomy;

	# VALIDATE
	$fields or $fields = [ 'taxon', 'rankId' ];
	foreach (@$fields) { $_ eq 'taxonomy' and confess("Disallowed recursion of taxonomy\n") }
	$delimiter or $delimiter = ':';
	$delimiter eq '/' and confess("Disallowed field delimiter: $delimiter\n");

#	# FILTER
#	if ( $ranks )
#	{
#		my @filtered;
#		if ( ref($ranks) )
#		{
#			ref($ranks) ne 'ARRAY' and confess("Invalid ranks parameter of type ".ref($ranks)."\n");
#			my %ranks;
#			foreach my $aRank (@$ranks)
#			{
#				exists($rankLevel{$aRank}) and $aRank = $rankLevel{$aRank};
#				$ranks{$aRank} = undef;
#			}
#			foreach (@taxonomy)
#			{
#				exists($ranks{$_->rankId}) and push @filtered, $_;
#			}
#		} elsif ( $ranks =~ /^major$/i )
#		{
#			foreach (@taxonomy)
#			{
#				exists($majorRanks{$_->rankId}) and push @filtered, $_;
#			}
#		} else
#		{
#			exists($rankLevel{$ranks}) and $ranks = $rankLevel{$ranks};
#			foreach (@taxonomy)
#			{
#				$_->rankId == $ranks and push @filtered, $_;
#			}
#		}
#		@taxonomy = @filtered;
#	}

	# FORMAT OUTPUT
	my @result;
	foreach my $aNode (@taxonomy)
	{
		my @fields = $aNode->record($fields);
		push @result, join($delimiter, @fields);
	}
	return join('/', @result);
}

=item nodesOfRank

Returns list of node objects with specified rank via depth-first pre-order traversal.

=cut

sub nodesOfRank
{
	my ($this, $rank) = @_;
	return $this->descend(
	{
		pretest => sub
		{
			my ($this, $rank) = @_;
			return $this->rankId > $$rank ? TRUE : FALSE;
		}, # don't traverse past query rank
		preorder => sub
		{
			my ($this, $rank) = @_;
			return $this->rankId == $$rank ? $this->rankId : ();
		}
	}, \$rank);
}

=item subtree

Output info for a subtree.  If the output file handle is not specified, write to STDOUT.  Optionally include test subroutine to decide which nodes to output (e.g. sub { return shift->rankName eq 'genus' ? 1:0 }).  Output is "preorder traversal" sorted.  Returns the number of nodes output.

=cut

sub subtree
{
	my ($this, $fields, $fh, $testSub) = @_;
	defined($fields) or confess("List of desired fields is required\n");
	unless (ref($fields))
	{
		my @fields = split(/,/, $fields);
		$fields = \@fields;
	}
	defined($fh) or $fh = *STDOUT;
	print $fh join("\t", '#taxon', @$fields), "\n";
	return $this->descend(
	{
		preorder => sub
		{
			my ($this, $fields, $fh, $testSub) = @_;
			defined($testSub) and ! &$testSub($this) and return 0;
			my @info = $this->info($fields);
			print $fh join("\t", $this->id, @info), "\n";
			return 1;
		},
		result => 'sum'
	}, $fields, $fh, $testSub);
}

=item ids

Returns list of all tax IDs at and below specified node via depth-first pre-order traversal.

=cut

sub ids
{
	return shift->descend({ preorder => sub { return shift->id } });
}

=item printIds

Prints list of all tax IDs at and below specified node via depth-first pre-order traversal.  If a file handle is not provided, writes to standard output.

=cut

sub printIds
{
	my ($this, $fh) = @_;
	defined($fh) or $fh = *STDOUT;
	$this->descend(
	{
		preorder => sub
		{
			my ($this, $fh) = @_;
			print $fh $this->id;
		}
	}, $fh);
}

=back

=head2 Helper Functions

=over 5

=item log

Placeholder method

=cut

sub log
{
	my ($this, $msg) = @_;
	chomp $msg;
	print STDERR $msg, "\n";
}

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2015 by the United States Department of Energy Joint Genome Institute

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

1;
