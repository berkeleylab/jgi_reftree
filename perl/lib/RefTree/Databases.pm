=pod

=head1 NAME

RefTree::Databases

=head1 DESCRIPTION

Extends the RefTree::Attributes class by adding support for databases (e.g. FASTA or tabular files).  The file offsets (for random access) are stored as attributes.

=head2 DATABASES

The following NCBI data is available via RefTree.  Use the listed db names to access.  User-supplied databases are also supported, but the files must be organized in the way the &aggregate method expects in order to be imported.  For each database, their is an associated attribute of the same name which stores the offset of the taxon's records within the datafile, for rapid random access.

=over 5

=item genomic

RefSeq "complete" genomic.

=item protein

RefSeq "complete" protein.

=item nonredundant_protein

RefSeq "complete" nonredundant_protein.

=item rna

RefSeq "complete" rna.

=item nt

Blast nonredundant nucleotide sequences.

=item nr

Blast nonredundant polypeptide sequences.

=back

In addition, user-supplied databases are supported.  They may contain FASTA or tabular data but must be imported and indexed by RefTree prior to use.

=cut

package RefTree::Databases;

use strict;
use Carp;
use File::Path qw(make_path remove_tree);
use Env qw(REFTREE_DIR TMPDIR);
use File::Slurp;
use File::Spec qw(rel2abs);
use DBI;
use constant
{
	FALSE => 0, TRUE => 1, # for readability
	FILE_MIN_SIZE => 40_000, # use for assigned file Ids
	NUM_PROCESSES => 16
};
use Parallel::ForkManager;
use parent qw(RefTree::Attributes);

# GLOBAL CLASS VARIABLES
our %availDbs; # { name => path } for all available DBs
our $maxFileId = 0; # used by dbm &mkTmpDirs
findDbs("$REFTREE_DIR/DATABASES");

=head1 METHODS

=head2 Constructors

=over 5

=item createTree

Extends this constructor inherited from its base class to assign and save file IDs (attribute).  For any database, a taxon's records will be in a file with the same ID (e.g. for taxon 22, it's genomic and protein records are in files genomic/22, protein/22); we don't bother to reassign file IDs for each database.

=cut

sub createTree
{
	my ($class, $taxdumpDir) = @_;
	my $this = $class->RefTree::Attributes::createTree($taxdumpDir);

	# DETERMINE TAXON SIZES USED FOR GROUPING RELATED TAXA INTO FILES
	# WE CURRENTLY USE THE NUMBER OF NR PROTEINS PER TAXON FOR THIS
	$this->log("Determining taxon sizes");
	$this->newAttribute('_size'); # temp attribute
	my $in;
	my $file = "$taxdumpDir/gi_taxid_prot.dmp";
	if ( -e "$file.gz" )
	{
		open($in, "gunzip -c $file.gz|") or confess("Unable to open gzipped file, $file.gz: $!");
	} else
	{
		open($in, '<', $file) or confess("Unable to open file, $file: $!");
	}
	my $taxon = undef;
	my $n = 0;
	my $node;
	while (<$in>)
	{
		chomp;
		my ($gi, $aTaxon) = split(/\t/);
		next unless $aTaxon;
		if ( $aTaxon == $taxon ) { ++$n }
		else
		{
			$node = $this->node($taxon) and $node->add('_size', $n);
			$taxon = $aTaxon;
			$n = 1;
		}
	}
	close($in);
	$node = $this->node($taxon) and $node->add('_size', $n);

	# ASSIGN FILE IDS TO NODES
	$this->log("Assigning file IDs");
	$this->newAttribute('file', "$REFTREE_DIR/ATTRIBUTES");
	$maxFileId = 0; # global class variable; file IDs start at 1
	$this->root->descend(
	{
		postorder => sub
		{
			my ($this, $fileIdSR) = @_;
			my $unassigned = $this->descend(
			{
				pretest => sub { return shift->has('file') },
				postorder => sub { return shift->get('_size') },
				result => 'sum'
			});
			if ( $unassigned >= FILE_MIN_SIZE )
			{
				$this->descend(
				{
					pretest => sub { return shift->has('file') },
					postorder => sub { @_[0]->set('file', @_[1]) }
				}, ++$$fileIdSR );
			}
		}
	}, \$maxFileId);
	# ASSIGN LAST REMAINING (i.e. < FILE_MIN_SIZE)
	$this->root->descend(
	{
		pretest => sub { return shift->has('file') },
		postorder => sub { @_[0]->set('file', @_[1]) }
	}, ++$maxFileId);
	$this->saveAttribute('file');
	return $this;
}

=back

=head2 File Management

=over 5

=item findDbs

Nonmember function discovers available databases in specified folders.

=cut

sub findDbs
{
	foreach (@_)
	{
		next if /^RefTree::Databases/;
		my $dir = File::Spec->rel2abs($_);
		-d $dir or next;
		opendir(DIR, $dir) or confess("Unable to opendir, $dir: $!");
		my @contents = grep { !/^\./ } readdir(DIR);
		closedir(DIR);
		foreach my $item (@contents)
		{
			my $path = "$dir/$item";
			if ( -d $path )
			{
				findDbs($path);
			} elsif ( $item =~ /^(.+)\.db$/ )
			{
				my $dbName = $1;
				$availDbs{$dbName} = $dir;
				RefTree::Attributes::findAttributes($dir);
			}
		}
	}
}

=item dbAvailable

Returns TRUE if the db is accessible (i.e. path known).  DBs in the $REFTREE_DIR folder are automatically discovered; other DBs must be explicitly defined using the &dbDir method.

=cut

sub dbAvailable
{
	my ($this, $dbName) = @_;
	$dbName or confess("DB name required\n");
	return exists($availDbs{$dbName}) ? TRUE : FALSE;
}

=item dbFile

Returns the path to the file containing the node.  The node doesn't necessarily have any records and the file doesn't necessarily exist.  Returns undef if the node doesn't have a file ID.

=cut

sub dbFile
{
	my ($this, $dbName) = @_;
	$dbName or confess("DB name required\n");
	my $fileId = $this->get('file') or confess("File ID not defined for ".$this->id."\n"); # this should never happen
	return join('/', $this->dbDir($dbName), $fileId);
}

=item dbTmpFile

Returns the path to the temporary file for a db.  An optional part parameter (e.g. $SGE_TASK_ID) may be provided if multiple processes may be generating temporary files for the same taxa.

=cut

sub dbTmpFile
{
	my ($this, $dbName, $part) = @_;
	my $fileId = $this->get('file') or confess("File ID not defined for ".$this->id."\n"); # this should never happen
	my $file = join('/', $this->dbTmpDir($dbName), $fileId, $this->id);
	if ( defined($part) )
	{
		$part =~ /^\d+$/ or confess("Part must be an integer\n");
		$file .= ".$part";
	}
	return $file;
}

=item newDb

Specify name and path for a new database.

=cut

sub newDb
{
	my ($this, $dbName, $dir) = @_;
	$dbName or confess("DB name required\n");
	$dbName =~ /^\w+$/ or confess("Invalid db name; no special chars or spaces allowed.\n");
	$dir or confess("DB folder required\n");
	$dir = File::Spec->rel2abs($dir);
	$this->log("New DB: $dbName at $dir");
	-d $dir and confess("New DB, $dbName, folder already exists: $dir\n");
	make_path($dir);
	$availDbs{$dbName} = $dir;
}

=item dbDir

Returns the data dir for the specified db or undef if not defined.

=cut

sub dbDir
{
	my ($this, $dbName) = @_;
	return $availDbs{$dbName};
}

=item dbTmpDir

Returns the temporary dir for the specified db.  This method does not validate or make the folder (see: &mkDbTmpDirs).

=cut

sub dbTmpDir
{
	my ($this, $dbName) = @_;
	$dbName or confess("DB name required\n");
	$TMPDIR or confess("\$TMPDIR not defined\n");
	return "$TMPDIR/RefTree.tmp/$dbName";
}

=item dbTmpFile

Returns the temporary file for the specified db and optional part.  This method does not validate or make the folder (see: &mkDbTmpDirs).

=cut

sub dbTmpFile
{
	my ($this, $dbName, $part) = @_;
	if ( !defined($part) )
	{
		$part = 1;
	} elsif ( $part !~ /^\d+$/ )
	{
		confess("Invalid part; must be an integer: $part\n");
	}
	my $dir = $this->dbTmpDir($dbName);
	my $fileId = $this->get('file');
	my $taxon = $this->id;
	return "$dir/$fileId/$taxon.$part";
}

=item mkDbTmpDirs

Create temporary folders used during loading.  This temporary organization creates too many inodes to serve as a persistent data structure; the data are later aggregated into a reasonable number of files, each containing many related taxa (see: &aggregate).  Such files are useful in their own right (i.e. for sequence similarity searches; see: reftreeSearch.pl).  If this method is called and the folder already exists, any contents are purged.  We create one folder per file ID in order to support parallel processing.  Files are concatenated by the &aggregate method.

=cut

sub mkDbTmpDirs
{
	my ($this, $dbName) = @_;
	$this->log("Make DB tmp dirs: $dbName");
	$dbName or confess("DB name required\n");
	my $tmpDir = $this->dbTmpDir($dbName);
	-d $tmpDir and system("rm -rf $tmpDir");
	# MAKE DIRECTORIES (ONE PER FILE ID)
	make_path($tmpDir) or confess("Unable to mkdir, $tmpDir: $!");
	my $max = $this->maxFileId;
	system("mkdir $tmpDir/{1..$max}") == 0 or confess("Error creating tmpDirs: $!");
	return $tmpDir;
}

=item maxFileId

Returns the max file id.  Used by &mkDbTmpDirs.  The global variable $maxFileId is set by the loadTree and createTree constructors, otherwise check the RDB file.

=cut

sub maxFileId
{
	my $this = shift;
	$maxFileId and return $maxFileId;
	my $file = "$REFTREE_DIR/ATTRIBUTES/file.db";
	-f $file or confess("File IDs have not been assigned; file does not exist: $file\n");
	my $dbh = DBI->connect("DBI:SQLite:$file") or confess("Unable to read SQLiteDb, $file: ".DBI->errstr."\n");
	my $sth = $dbh->prepare("SELECT max(value) FROM file") or confess("Couldn't prepare statement: ".$dbh->errstr);
	$sth->execute;
	my @row = $sth->fetchrow_array;
	$maxFileId = shift @row or confess("Unable to retrieve max file ID from $file\n");
	return $maxFileId;
}

=item aggregate

Temporary files are aggregated into a resonable number of files, each containing records of related taxa.  The index (flat text and sqlite rdb) files are also created.  Requires db name and directory.  Files are required to be in the expected location and be named appropriately -- there is one folder per file ID, there may be many files therein to be concatenated, they are named by taxon.  There may be multiple files per taxon, each generated by a different worker process, in which case, the taskId should be appended to the filename.

=cut

sub aggregate
{
	my ($this, $dbName, $keep) = @_;

	# INIT DIRS
	my $outDir = $this->dbDir($dbName) or confess("Unknown db: $dbName\n");
	my $inDir = $this->dbTmpDir($dbName);
	-d $inDir or confess("No tmpdir exists for $dbName\n");
	$this->log("Aggregate DB: $dbName (in=$inDir, out=$outDir)");
	-d $outDir and system("rm -rf $outDir");
	make_path($outDir) or confess("Unable to mkdir $outDir: $!");

	# INPUT FOLDERS (ONE PER FILE ID)
	opendir(DIR, $inDir) or confess("Unable to read dir, $inDir: $!");
	my @folders = sort { $a <=> $b } grep { /^\d+$/ } readdir(DIR);
	closedir(DIR);
	@folders or confess("No tmpdirs exist for $dbName\n");

	# CONCATENATE EACH FOLDER'S TMPFILES AND RECORD OFFSETS
	my $pm = Parallel::ForkManager->new(NUM_PROCESSES);
	foreach my $fileId (@folders)
	{
		# Each fileId has a tmp folder and has files named "<taxon>.<part>"
		my $tmpDir = "$inDir/$fileId";
		opendir(DIR, $tmpDir) or confess("Unable to read dir, $tmpDir: $!");
		my @tmpFiles = sort { $a <=> $b } grep { /^\d+\.\d+$/ } readdir(DIR);
		closedir(DIR);
		next unless @tmpFiles;
		$this->log("Aggregating $dbName file $fileId");
		$pm->start and next;
		my $outFile = "$outDir/$fileId";
		my $offset = 0;
		my $taxon = undef;
		open(my $out, '>', $outFile) or confess("Unable to write $outFile: $!");
		open(my $index, '>', "$outFile.idx") or confess("Unable to write $outFile.idx: $!");
		foreach my $tmpFile (@tmpFiles)
		{
			$tmpFile =~ /^(\d+)\.\d+$/ or confess($!);
			my $aTaxon = $1;
			$tmpFile = "$tmpDir/$tmpFile";
			open(my $in, '<', $tmpFile) or confess("Unable to read $tmpFile: $!");
			while (<$in>)
			{
				next if /^#/; # ignore comment line
				$taxon == $aTaxon or print $index join("\t", $taxon = $aTaxon, $offset), "\n";
				print $out $_;
				$offset += length($_);
			}
			close($in);
			$keep or unlink($tmpFile);
		}
		close($out);
		close($index);
		$pm->finish;
	}
	$pm->wait_all_children;

	# POPULATE NODES WITH OFFSET ATTRIBUTE, RM TMP INDEX FILES, AND
	# SAVE OFFSET ATTRIBUTE (FLAT AND RDB FILES)
	$this->log("Preparing $dbName index");
	$this->newAttribute($dbName, $outDir);
	opendir(DIR, $outDir) or confess("Unable to read dir, $outDir: $!");
	my @files = sort { $a <=> $b } grep { /^\d+$/ } readdir(DIR);
	closedir(DIR);
	foreach my $fileId (@files)
	{
		my $file = "$outDir/$fileId.idx";
		open(my $in, '<', $file) or confess("Unable to read $file: $!");	
		while (<$in>)
		{
			chomp;
			my ($taxon, $offset) = split(/\t/);
			my $node = $this->node($taxon) or confess("Unknown taxon: $taxon\n");
			$node->set($dbName, $offset);
		}
		close($in);
		unlink($file);
	}
	$this->saveAttribute($dbName);
}

=back

=head2 Output Methods

=over 5

=item output

Output the record(s) of this node to the file handle.  To output DB records, the DB name is required.  Returns number of records output (a node may have zero or more records in a DB).

=cut

sub output
{
	my ($this, $fh, $fields, $dbName) = @_;

	# DELEGATE IF NO DB
	defined($dbName) or return $this->RefTree::Attributes::output($fh, $fields);

	# VALIDATE INPUT
	defined($fh) or $fh = *STDOUT;
	$this->dbAvailable($dbName) or confess("Unknown Db: $dbName\n");

	# CHECK IF THIS NODE HAS ANY RECORDS IN DB
	my $offset = $this->get($dbName);
	defined($offset) or return 0;

	# GET RECORD/ATTRIBUTE INFO
	my $info = {};
	$info = $this->record($fields);
	my @info;
	push @info, "$_=".$info->{$_} foreach sort keys %$info;

	# GET DB RECORD -- ONLY FASTA, TABULAR FORMATS SUPPORTED
	my $counter = 0;
	my $file = $this->dbFile($dbName);
	open(my $in, '<', $file) or confess("Unable to read file, $file: $!");
	seek($in, $offset, 0);
	my $line = <$in>;
	if ($line =~ /^>/)
	{
		# FASTA DB
		do
		{
			if ( $line =~ /^(>\S+) (taxid=(\d+).*)$/)
			{
				my $hdr = $1;
				my $taxon = $3;
				my @features = ( @info, split(/,/, $2) );
				if ( $taxon == $this->id )
				{
					++$counter;
					print $fh $hdr, ' ', join(',', @features), "\n";
				}
				elsif ( $counter )
				{
					close($in);
					return $counter;
				}
			}
			elsif ( $counter ) { print $fh $line }
		}
		while ($line = <$in>);
	}
	elsif ( $line =~ /^\d+\t/ )
	{
		# FLAT-TEXT/TSV DB
		do
		{
			chomp $line;
			my ($taxon, @data) = split(/\t/, $line);
			if ( $taxon == $this->id )
			{
				++$counter;
				print $fh join("\t", $taxon, @info, @data), "\n";
			}
			elsif ( $counter )
			{
				close($in);
				return $counter
			}
		}
		while ($line = <$in>);
	}
	else
	{
		confess("Invalid file format; got line:\n$line\n");
	}
	close($in);
	return $counter;
}

=back

=head1 SEE ALSO

C<RefTree::Taxonomy>, C<RefTree::Attributes>

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2015 by the United States Department of Energy Joint Genome Institute

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

1;

__END__

# TODO ALLOW IMPORTING OF DATA THAT'S NOT IN THE EXPECTED STRUCTURE

=back

=head1 User Data

=item importDb

Load a database (i.e. user data).  Supported formats are FASTA (requires string "tax|<taxonId>" or "tax=<taxonId>" in header) and tabular (requires taxon ID as first column).  Reads from either a file or standard input.

=cut

sub importDb
{
	my ($this, $inDir, $outDir) = @_;
	die("NYI\n"); # ECCE
	$inDir or confess("Input directory required\n");
	$outDir or confess("Output directory required\n");
	-d $inDir or confess("Input folder does not exist: $inDir\n");
	-d $outDir or make_path($outDir) or confess("Unable to mkdir $outDir: $!\n");
	$this->_importDb($inDir, $outDir);
}

=item _importDb

Private method used by importDb.  Process one input folder.  Recursive.

=cut

# TODO
sub _importDb
{
	my ($this, $inDir, $outDir) = @_;
	opendir(DIR, $inDir) or confess("Unable to read dir, $inDir: $!");
	my @contents = grep { !/\./ } readdir(DIR);
	closedir(DIR);
	my $this;
	foreach (@contents)
	{
		my $path = "$inDir/$_";
		if ( -d $path )
		{
			$this->_importDb($inDir, $outDir);
		} elsif ( -f $path and /^(\d+)/ )
		{
			if ( $this = $this->node($1) )
			{
				my $fileId = $this->get('file') or confess("File ID not defined for $_");
				my $outFile = "$outDir/$fileId";
				$this->log("Aggregating $path");
				open(my $in, '<', $path) or confess("Unable to read $path: $!");
				open(my $out, '>>', $outFile) or confess("Unable to append $outFile: $!");
				while (<$in>) { print $out $_ }
				close($in);
				close($out);
			} else
			{
				warn("Skipping unknown taxon ID: $1\n");
			}
		} else
		{
			warn("Skipping invalid: $path\n");
		}
	}
}

# TODO -- IF FILE IDS CHANGE, USER DBS MUST BE REORGANIZED.  NCBI DBS WILL BE RELOADED, SO THIS IS ONLY FOR USER DBS.

=item reaggregate

As NCBI data changes (in particular, currently proteins/taxa in nr), the fileId assignments may change.  This function updates the files (in place).

=cut

sub reaggregate
{
	my ($this, $dbName) = @_;

	# SET PATH
	my $dir = $this->dbDir($dbName);
	-d $dir or confess("Invalid dir for $dbName: $dir");

	# DETERMINE FILES TO PROCESS
	opendir(DIR, $dir) or confess("Unable to read dir, $dir: $!");
	my @files = sort { $a <=> $b } grep { /^\d+$/ } readdir(DIR);
	closedir(DIR);

	# DETERMINE FILE FORMAT
	my $file;
	my $format;
	foreach $file ( @files )
	{
		my $path = "$dir/$file";
		-s $path or next;
		open(my $in, '<', $path) or confess("Unable to read file, $path: $!");
		my $line = <$in>;
		close($in);
		if ( $line =~ /^>(\S+)/ ) { $format = 'fasta' }
		elsif ( $line =~ /^\d+\t/ ) { $format = 'tabular' }
		else { confess("Invalid format for file: $path") }
		last;
	}
	$format or confess("All files are empty");

	# CREATE TMPDIRS
	my $outDir = $this->mkDbTmpDirs($dbName);

	# SPLIT OLD FILES
	my $pm = Parallel::ForkManager->new(NUM_PROCESSES);
	for ( my $i=0; $i<=$#files; $i++ )
	{
		$pm->start and next;
		my $fileId = $files[$i];
		-e "$dir/$file.idx" and unlink("$dir/$file.idx");
		my $inFile = "$dir/$fileId";
		open(my $in, '<', $inFile) or confess("Unable to read file, $inFile: $!");
		my $out;
		my $currentOutfile;
		my $taxon;
		my $this;
		my $newFileId;
		my $outFile;
		if ( $format eq 'tabular' )
		{
			my $line;
			while ( $line = <$in>)
			{
				($taxon) = split(/\t/, $line);
				next unless $taxon;
				unless ( $this = $this->node($taxon) )
				{
					warn("Skipping unknown taxon: $taxon\n");
					next;
				}
				$newFileId = $this->get('file');
				$outFile = "$outDir/$newFileId.$i";
				if ( $outFile ne $currentOutfile )
				{
					defined($out) and close($out);
					open($out, '>>', $outFile) or confess("Unable to append outFile, $outFile: $!");
				}
				print $out $line;
			}
		} else
		{
			while (<$in>)
			{
				if (/^>gi\|\d+\|\w+\|[\w._]+ taxid=(\d+)/)
				{
					unless ( $this = $this->node($taxon = $1) )
					{
						warn("Skipping unknown taxon: $taxon\n");
					}
					$newFileId = $this->get('file');
					$outFile = "$outDir/$newFileId.$i";
					if ( $outFile ne $currentOutfile )
					{
						defined($out) and close($out);
						open($out, '>>', $outFile) or confess("Unable to append outFile, $outFile: $!");
					}
				} elsif (/^>/)
				{
					confess("Unable to reaggregate db; invalid FASTA header: $inFile\n");
				}
				print $out $_;
			}
		}
		close($in);
		defined($out) and close($out);
		unlink($inFile);
		$pm->finish;
	}
	-e "$dir/version" and unlink("$dir/version");
	$pm->wait_all_children;
	system("rm -rf $dir");

	# AGGREGATE FILES
	$this->aggregate($dbName);
}

