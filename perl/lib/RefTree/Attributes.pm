=pod

=head1 NAME

RefTree::Attributes

=head1 DESCRIPTION

Extends the RefTree::Taxonomy class with attributes, which are also stored in the node object (i.e. RAM).

=cut

package RefTree::Attributes;

use strict;
use Carp;
use Env qw(REFTREE_DIR TMPDIR);
use constant { FALSE => 0, TRUE => 1 };
use File::Path qw(make_path remove_tree);
use DBI;
use Contextual::Return;
use parent qw(RefTree::Taxonomy);
use constant
{
	FALSE => 0, TRUE => 1, # for readability
	ATTRIBUTES => 4 # index on Node object array
};

# GLOBAL VARIABLES
our $REFTREE_ATTRIBUTES_DIR = "$REFTREE_DIR/ATTRIBUTES";
our %availAttributes; # { key => dir } for all available attributes
-d "$REFTREE_DIR/ATTRIBUTES" and findAttributes("$REFTREE_DIR/ATTRIBUTES");
our %loadedAttributes; # { key => dir } for all loaded attributes

=head1 METHODS

=over 5

=item newNode

Create a new node object.

=cut

sub newNode
{
	my $class = shift;
	my $this = $class->RefTree::Taxonomy::newNode(@_);
	$this->[ATTRIBUTES] = {};
	if ( $this->lazy )
	{
		# WITH LAZY INITIATION, WE SHOULD LOAD ALL ATTRIBUTES BEING USED
		$this->get($_, $loadedAttributes{$_}) foreach keys %loadedAttributes;
	}
	return $this;
}

=item createTree

Extends createTree of parent class to add support for scientific name and genetic code attributes ('name', 'code').

=cut

sub createTree
{
	my ($class, $taxdumpDir) = @_;
	my $this = $class->RefTree::Taxonomy::createTree($taxdumpDir);

	# LOAD GENETIC CODE ATTRIBUTE
	$this->log("Loading genetic codes");
	$this->newAttribute('code', $REFTREE_ATTRIBUTES_DIR);
	print "Reading nodes.dmp\n";
	my $in;
	my $file = "$taxdumpDir/nodes.dmp";
	if ( -f "$file.gz" )
	{
		open($in, "gunzip -c $file.gz |") or confess("Unable to read gzipped file, $file.gz: $!");
	} else
	{
		open($in, '<', $file) or confess("Unable to read file, $file: $!");
	}
	while (<$in>)
	{
		my ($taxon, $parentId, $rankName, $emblCode, $divisionId, $inherited, $code) = split(/\t\|\t/);
		my $node = $this->node($taxon) or confess("Unknown taxon: $taxon\n");
		$node->set('code', $code);
	}
	close($in);
	$this->saveAttribute('code');

	# LOAD NAMES ATTRIBUTE
	$this->log("Loading scientific names");
	$this->newAttribute('name', $REFTREE_ATTRIBUTES_DIR);
	print "Reading names.dmp\n";
	$file = "$taxdumpDir/names.dmp";
	my $node;
	if ( -f "$file.gz" )
	{
		open($in, "gunzip -c $file.gz |") or confess("Unable to read gzippde file, $file.gz: $!");
	} else
	{
		open($in, '<', $file) or confess("Unable to read file, $file: $!");
	}
	while (<$in>)
	{
		if (/scientific name/)
		{
			my ($taxon, $name) = split(/\t\|\t/);
			my $node = $this->node($taxon) or confess("Unknown taxon: $taxon\n");
			$node->set('name', $name);
		}
	}
	close($in);
	$this->saveAttribute('name');
	return $this;
}
=back

=head2 Accessors

=over 5

=item newAttribute

Initialize a new attribute.  New attributes must be initialized before they may be used.  An output folder is not required for temporary attributes.

=cut

sub newAttribute
{
	my ($this, $key, $dir) = @_;
	if ( $this->attributeLoaded($key) )
	{
		confess("Attribute $key already loaded\n");
	} elsif ( $this->attributeAvailable($key) )
	{
		confess("Attribute $key already exists\n");
	} elsif ( $dir and -f "$dir/$key.dmp" )
	{
		confess("Attribute $key already exists\n");
	}
	$loadedAttributes{$key} = $dir;
}

=item set

Set a node's attribute (key-value pair).  Value may be undef.

=cut

sub set
{
	my ($this, $key, $value) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute, $key\n");
	$this->[ATTRIBUTES]->{$key} = $value;
}

=item get

Return a node's attribute, which may be undef.  Also allows access to I<rankName> and I<rankId> as if they were attributes even though they are not.

=cut

sub get
{
	my ($this, $key) = @_;
	if ( $key eq 'rankName' ) { return $this->rankName }
	elsif ( $key eq 'rankId' ) { return $this->rankId }
	elsif ( $this->attributeLoaded($key) ) { 1 }
	elsif ( $this->attributeAvailable($key) ) { $this->loadAttribute($key) }
	else { confess("Unknown attribute, $key\n") }
	return $this->[ATTRIBUTES]->{$key};
}

=item add

Add a numeric value to attribute.

=cut

sub add
{
	my ($this, $key, $value) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute, $key\n");
	return $this->[ATTRIBUTES]->{$key} += $value;
}

=item subtract

Subtract a numeric value to attribute.

=cut

sub subtract
{
	my ($this, $key, $value) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute, $key\n");
	return $this->[ATTRIBUTES]->{$key} -= $value;
}

=item attributeLoaded

Returns TRUE if key is known (i.e. has been loaded or created this session).

=cut

sub attributeLoaded
{
	my ($this, $key) = @_;
	$key or confess("A valid key is required\n");
	return exists($loadedAttributes{$key}) ? TRUE : FALSE;
}

=item has

Check if a node's attributes includes specified key.

=cut

sub has
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute key: $key\n");
	return exists($this->[ATTRIBUTES]->{$key}) ? TRUE : FALSE;
}

=item delete

Delete a node's key-value pair.

=cut

sub delete
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute key: $key\n");
	delete($this->[ATTRIBUTES]->{$key});
}

=item clear

Clear all attributes from a node.

=cut

sub clear
{
	my $this = shift;
	$this = {};
}

=back

=head2 File Management

=over 5

=item findAttributes

Nonmember function discovers available databases in specified folders.

=cut

sub findAttributes
{
	foreach my $dir (@_)
	{
		opendir(DIR, $dir) or confess("Unable to opendir, $dir: $!");
		my @contents = grep { !/^\./ } readdir(DIR);
		closedir(DIR);
		foreach my $item (@contents)
		{
			my $path = "$dir/$item";
			if ( -d $path )
			{
				findAttributes($path);
			} elsif ( $item =~ /^(.+)\.db$/ )
			{
				my $key = $1;
				$availAttributes{$key} = $dir;
			}
		}
	}
}

=item attributeAvailable

Returns TRUE if the attribute has been loaded or may be automatically loaded.

=cut

sub attributeAvailable
{
	my ($this, $key) = @_;
	return exists($availAttributes{$key}) ? TRUE : FALSE;
}

=item attributeDir

Returns the folder containing the attribute files.

=cut

sub attributeDir
{
	my ($this, $key) = @_;
	$this->attributeAvailable($key) or confess("Unknown attribute: $key\n");
	return $availAttributes{$key};
}

=item loadAttribute

Load or an existing attribute from RefTree dump or database file.  Returns the number of nodes with defined values.

=cut

sub loadAttribute
{
	my ($this, $key, $dir) = @_;
	$this->attributeLoaded($key) and confess("Attribute $key has already been loaded\n");
	if ( $dir )
	{
		if ( $this->attributeAvailable($key) )
		{
			my $currDir = $this->attributeDir($key);
			if ( $currDir ne $dir )
			{
				confess("Attribute \"$key\" already exists in $currDir\n");
				# TODO OPTION TO CREATE COPY OF AN ATTRIBUTE WITH A NEW NAME AND DIR
			}
		} else
		{
			$availAttributes{$key} = $dir;
		}
	} elsif ( $this->attributeAvailable($key) )
	{
		$dir = $this->attributeDir($key);	
	} else
	{
		confess("Unknown attribute \"$key\"\n");
	}
	$this->log("Load attribute \"$key\" from $dir\n");
	$loadedAttributes{$key} = $dir;

	# LOAD ATTRIBUTE FROM RDB FOR INITIALIZED NODES ONLY
	if ( $this->lazy )
	{
		my $file = "$dir/$key.db";
		-f $file or confess("Attribute $key not found at $file\n");
		my $dbh = DBI->connect("DBI:SQLite:$file") or confess("Unable to read SQLiteDb, $file: ".DBI->errstr."\n");
		my $sth = $dbh->prepare_cached("SELECT value FROM $key WHERE taxon = ?") or confess("Couldn't prepare statement: ".$dbh->errstr);
		my $n = 0;
		foreach my $node ( values %{$this->nodes} )
		{
			$sth->execute($node->id);
			my @row = $sth->fetchrow_array;
			my $value = shift @row;
			defined($value) and $node->[ATTRIBUTES]->{$key} = $value;
			++$n;
		}
		return $n;
	}

	# LOAD ATTRIBUTE FROM SORTED FLATFILE FOR ENTIRE TREE
	my $file = "$dir/$key.dmp";
	open(my $fh, '<', $file) or confess("Unable to open attribute file, $file: $!\n");
	my $dbVers = <$fh>;
	$dbVers =~ /^#REFTREE=(\w+)/ or confess("Invalid attribute file, $file\n");
	$dbVers = $1;
	if ( $dbVers ne $this->version )
	{
		# OLD FILE IDS SO UPDATE DMP FILE
		$this->log("Updating attribute \"$key\" to current RefTree version\n");
		close($fh);
		move($file, "$file.old") or confess("Unable to move file, $file: $!");
		delete($loadedAttributes{$key});
		my $n = $this->importAttribute($key, $dir, "$file.old");
		unlink("$file.old");
		return $n;
	}

	# TRAVERSE AND LOAD
	my $line = <$fh>;
	$line =~ /^(\d+)\t(.+)$/ or confess("Invalid record: $line\n");
	my $taxon = $1;
	my $value = $2;
	return $this->root->descend(
	{
		#pretest => sub { return $$taxon ? FALSE : TRUE }, # eof
		preorder => sub
		{
			my ($this, $key, $fh, $taxon, $value) = @_;
			$$taxon == $this->id or return 0; # some nodes may not have a value
			$this->set($key, $$value);
			my $line = <$fh>;
			if ( $line )
			{
				$line =~ /^(\d+)\t(.+)$/ or confess("Invalid record: $line\n");
				$$taxon = $1;
				$$value = $2;
			} else
			{
				$$taxon = undef;
				$$value = undef;
			}
			return 1;
		},
		result => 'sum'
	}, $key, $fh, \$taxon, \$value);
}

=item saveAttribute

Writes all values of an attribute (i.e. for the entire tree) to a flatfile and relational database.  Returns the number of nodes which have the attribute (including those with undef values).

=cut

sub saveAttribute
{
	my ($this, $key) = @_;
	$this->log("Save attribute, \"$key\"");
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	my $dir = $loadedAttributes{$key};
	defined($dir) or confess("Output dir not defined for attribute $key");
	-d $dir or make_path($dir) or confess("Unable to mkdir $dir: $!");

	# FLAT-FILE
	my $txtFile = "$dir/$key.dmp";
	open(my $fh, '>', $txtFile) or confess("Unable to write file, $txtFile: $!");
	print $fh '#REFTREE=',$this->version,"\n";

	# SQLITE3 RDB
	my $dbFile = "$dir/$key.db";
	-f $dbFile and unlink($dbFile);
	my $dbh = DBI->connect("DBI:SQLite:$dbFile", { RaiseError => 1 }) or confess(DBI->errstr);
	$dbh->{AutoCommit} = 0;
	$dbh->do("CREATE TABLE $key ( taxon INTEGER PRIMARY KEY, value INTEGER )");
	my $sth = $dbh->prepare_cached("INSERT INTO $key ( taxon, value ) VALUES ( ?, ? )");
	my $counter = 0; # group inserts into transactions

	# TRAVERSE
	my $n = $this->root->descend(
	{
		preorder => sub
		{
			my ($this, $key, $fh, $dbh, $sth, $counterSR) = @_;
			$this->has($key) or return 0;
			my @row = ( $this->id, $this->get($key) );
			print $fh join("\t", @row), "\n";
			$sth->execute(@row);
			++$$counterSR % 10_000 or $dbh->commit;
			return 1;
		},
		action => 'sum'
	}, $key, $fh, $dbh, $sth, \$counter);
	$dbh->commit;
	$sth->finish;
	return $n;
}

=item importAttribute

Load attribute from unsorted tabular file or standard input.

=cut

sub importAttribute
{
	my ($this, $key, $outDir, $inFile) = @_;
	$this->log("Import attribute, \"$key\"\n");
	$key or confess("Key required\n");
	$key =~ /^[_.]/ and confess("User attribute keys may not begin with an underscore or dot\n");
	$outDir or confess("Output folder required\n");
	$this->createAttribute($key, $outDir);
	-d $outDir or make_path($outDir) or confess("Unable to mkdir $outDir: $!\n");
	my $in;
	if ( $inFile )
	{
		open($in, '<', $inFile) or confess("Unable to read file, $inFile: $!\n");
	} else
	{
		$in = *STDIN;
	}
	my $n = 0;
	while (<$in>)
	{
		chomp;
		next if /^#/;
		/^(\d+)\t(.+)$/ or confess("Invalid line: $_\n");
		my $taxon = $1;
		my $value = $2;
		my $node = $this->node($taxon) or confess("Unknown taxon: $taxon\n");
		$node->set($key, $value);
		++$n;
	}
	$this->saveAttribute($key);
	return $n;
}

=back

=head2 Attribute Traversals

=over 5

=item numHave

Returns number of nodes with the attribute.

=cut

sub numHave
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	return $this->descend(
	{
		preorder => sub 
		{
			my ($this, $key) = @_;
			return $this->has($key) ? TRUE : FALSE;
		},
		result => 'sum'
	}, $key);
}

=item any

Returns TRUE if any node in the subtree has the specified attribute; FALSE otherwise.

=cut

sub any
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	return $this->descend(
	{
		preorder => sub 
		{
			my ($this, $key) = @_;
			return $this->has($key) ? TRUE : FALSE;
		},
		posttest => sub
		{
			my ($this, $result) = @_;
			return $result ? TRUE : FALSE
		},
		result => 'sum'
	}, $key);
}

=item all

Returns TRUE if all nodes in the subtree have the specified attribute; FALSE otherwise.

=cut

sub all
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	return $this->descend(
	{
		preorder => sub
		{
			my ($key, $this) = @_;
			return $this->has($key) ? FALSE : TRUE;
		},
		posttest => sub
		{
			my ($this, $result) = @_;
			return $result ? FALSE : TRUE;
		},
		result => 'sum'
	}, $key );
}

=item deleteAll

Delete an attribute from every node in a subtree.

=cut

sub deleteAll
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute key: $key\n");
	$this->descend(
	{
		preorder => sub { @_[0]->delete(@_[1]) }
	}, $key );
	delete($loadedAttributes{$key});
}

=item clearAll

Clear all attributes from a subtree.

=cut

sub clearAll
{
	my $this = shift;
	%loadedAttributes = ();
	return $this->descend(
	{
		preorder => sub { shift->clear }
	});
}

=item sum

Returns the sum of some attribute in a subtree.

=cut

sub sum
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	return $this->descend(
	{
		result => 'sum',
		preorder => sub 
		{
			my ($this, $key) = @_;
			return $this->get($key);
		}
	}, $key );
}

=item min

Returns the minimum defined value of some attribute in a subtree.

=cut

sub min
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	my $result;
	$this->descend(
	{
		preorder => sub 
		{
			my ($this, $key, $result) = @_;
			my $x = $this->get($key);
			defined($x) or return;
			if ( !defined($$result) or $x < $$result ) { $$result = $x }
		}
	}, $key, \$result );
	return $result;
}

=item max

Returns the maximum defined value of some attribute in a subtree.

=cut

sub max
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	my $result;
	$this->descend(
	{
		preorder => sub 
		{
			my ($this, $key, $result) = @_;
			my $x = $this->get($key);
			defined($x) or return;
			if ( !defined($$result) or $x > $$result ) { $$result = $x }
		}
	}, $key, \$result );
	return $result;
}

=item mean

In scalar context, returns the mean defined value of some attribute in a subtree.  In list context, returns the mean and standard deviation.

=cut

sub mean
{
	my ($this, $key) = @_;
	$this->attributeLoaded($key) or confess("Unknown attribute: $key");
	my $sum = 0;
	my $n = 0;
	$this->descend(
	{
		preorder => sub 
		{
			my ($this, $key, $sum, $n) = @_;
			my $x = $this->get($key);
			defined($x) or return;
			++$$n;
			$$sum += $x;
		}
	}, $key, \$sum, \$n );
	my $mean = $sum/$n;
	wantarray or return $mean;

	# CALC STANDARD DEVIATION
	$sum = 0;
	$this->descend(
	{
		preorder => sub 
		{
			my ($this, $key, $sum, $mean) = @_;
			my $x = $this->get($key);
			defined($x) or return;
			$$sum += ( $x - $$mean ) ** 2;
		}
	}, $key, \$sum, \$mean );
	my $stdev = sqrt($sum/($n-1));
	return ($mean, $stdev);
}

=back

=head2 Output Methods

=over 5

=item field

Returns a field value(s) given the keyword/name or undef if unknown.

=cut

sub field
{
	my ($this, $field) = @_;
	if ( $this->attributeAvailable($field) ) { return $this->get($field) }
	else { return $this->RefTree::Taxonomy::field($field) }
}

=item record

Returns either array, arrayref, hashref, or (comma-separated) string of requested fields (if none specified, default is taxonomy).

=cut

# TODO ADD ADDITIONAL TAXONOMY FORMATTING OPTIONS
sub record
{
	my ($this, $fields) = @_;
	unless ( defined($fields) and @$fields > 0 ) { $fields = [ 'taxonomy' ] }
	my @result;
	my %result;
	foreach ( @$fields )
	{
		push @result, $result{$_} = $this->field($_);
	}
	return
		SCALAR { join(',', @result) }
		LIST { @result }
		ARRAYREF { \@result }
		HASHREF { \%result };
}

=back

=head1 SEE ALSO

C<RefTree::Taxonomy>

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2015 by the United States Department of Energy Joint Genome Institute

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

1;

__END__

# TODO ADD SUPPORT FOR ATTRIBUTE LISTS AND HASHES
=item ref

Return a node attribute's reference.

=cut

sub ref
{
	my ($this, $key) = @_;
	defined($key) or confess("Key required\n");
	$this->attributeLoaded($key) or confess("Unknown attribute, $key (must load or define first)\n");
	return \{$this->[ATTRIBUTES]->{$key}};
}

=item push

Add a datum to the end of a node's attribute list.

=cut

sub push
{
	my ($this, $key, $value) = @_;
	$key or confess("Key required\n");
	my $ar = $this->[ATTRIBUTES]->{$key};
	ref($ar) or confess("Attribute $key is not a list\n");
	push @$ar, $value;
}

=item unshift

Add a datum to the begining of a node's attribute list.

=cut

sub unshift
{
	my ($this, $key, $value) = @_;
	$key or confess("Key required\n");
	my $ar = $this->[ATTRIBUTES]->{$key};
	ref($ar) or confess("Attribute $key is not a list\n");
	unshift @$ar, $value;
}

=item shift

Remove and return the first item of a node's attribute list.

=cut

sub shift
{
	my ($this, $key) = @_;
	$key or confess("Key required\n");
	my $ar = $this->[ATTRIBUTES]->{$key};
	ref($ar) or confess("Attribute $key is not a list\n");
	return shift @$ar;
}

=item pop

Remove and return the last item of a node's attribute list.

=cut

sub pop
{
	my ($this, $key) = @_;
	$key or confess("Key required\n");
	my $ar = $this->[ATTRIBUTES]->{$key};
	ref($ar) or confess("Attribute $key is not a list\n");
	return pop @$ar;
}

