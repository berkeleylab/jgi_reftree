#!/usr/bin/env perl

use strict;
use constant { BLAST_PART_SIZE => 1_000_000 };
use Getopt::Long;
use Pod::Usage;
use Parallel::ForkManager;
use Env qw(REFTREE_DIR TMPDIR);
use RefTree;

my ($taxdumpDir, $blastDir, $refseqDir, $help, $man);
GetOptions
(
	'taxdump=s' => \$taxdumpDir,
	'blast=s' => \$blastDir,
	'refseq=s' => \$refseqDir,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$taxdumpDir or die("--taxdump dir required\n");
$taxdumpDir = File::Spec->rel2abs($taxdumpDir);
-d $taxdumpDir or die("Invalid taxdump dir: $taxdumpDir\n");
$blastDir or die("--blast dir required\n");
$blastDir = File::Spec->rel2abs($blastDir);
-d $blastDir or die("Invalid blast dir: $blastDir\n");
$refseqDir or die("--refseq dir required\n");
$refseqDir = File::Spec->rel2abs($refseqDir);
-d $refseqDir or die("Invalid refseq dir: $refseqDir\n");

# CREATE TREE
our $tree = RefTree->createTree($taxdumpDir);
our $pm = new Parallel::ForkManager(16);

# IMPORT
importBlast($tree, $taxdumpDir, $blastDir);
importRefSeq($tree, $refseqDir);
exit;

sub importBlast
{
	my ($tree, $taxdumpDir, $blastDir) = @_;

	# PARSE PART FILES
	foreach my $dbName ( qw(nt nr) )
	{
		# DEFINE PATHS
		my $gi2taxFile;
		my $blastFastaFile;
		if ( $dbName eq 'nt' )
		{
			$gi2taxFile = "$taxdumpDir/gi_taxid_nucl.dmp";
			$blastFastaFile = "$blastDir/nt.gz";
		} elsif ( $dbName eq 'nr' )
		{
			$gi2taxFile = "$taxdumpDir/gi_taxid_prot.dmp";
			$blastFastaFile = "$blastDir/nr.gz";
		} else
		{
			die("Invalid Blast DB name: $dbName\n");
		}

		# MAKE TMPDIR
		$tree->newDb($dbName, "$REFTREE_DIR/DATABASES/$dbName");
		my $tmpDir = $tree->mkDbTmpDirs($dbName);

		# SPLIT DB
		my ($gi2taxFiles, $fastaFiles) = splitBlastDb($tree, $dbName, $gi2taxFile, $blastFastaFile, $tmpDir);

		# PROCESS PARTS
		my $i1 = $#$fastaFiles;
		for ( my $i=0; $i<=$i1; $i++ )
		{
			$tree->log("Loading Blast $dbName part $i of $i1");
			$gi2taxFiles->[$i] or next;
			$fastaFiles->[$i] or next;
			$pm->start and next;
			readBlastFastaFile($tree, $dbName, $gi2taxFiles->[$i], $fastaFiles->[$i], $i);
			unlink($gi2taxFiles->[$i], $fastaFiles->[$i]);
			$pm->finish;
		}
		$pm->wait_all_children;
		$tree->aggregate($dbName);
		system("rm -rf $tmpDir");
	}
}

# Split a compressed blast DB into parts by GI.
sub splitBlastDb
{
	my ($tree, $dbName, $gi2taxFile, $blastFastaFile, $tmpDir) = @_;

	# VALIDATE
	$dbName or die("DB name required");
	$gi2taxFile or die("GI:tax file required\n");
	$blastFastaFile or die("Blast DB file required\n");
	$tmpDir or die("Temporary output dir required\n");
	-d $tmpDir or make_path($tmpDir) or die("Unable to make path, $tmpDir: $!");

	# SPLIT GI:TAX FILE BY GI AND DETERMINE HIGHEST GI
	# (gi:tax FILE IS SORTED BY GI)
	$tree->log("Splitting $dbName GI:tax file");
	my $in;
	if ( -f "$gi2taxFile.gz" )
	{
		open($in, "gunzip -c $gi2taxFile.gz |") or die("Unable to read gzipped file, $gi2taxFile.gz: $!");
	} elsif ( -f $gi2taxFile )
	{
		open($in, '<', $gi2taxFile) or die("Unable to read file, $gi2taxFile: $!");
	} else
	{
		die("GI:tax file not found: $gi2taxFile\n");
	}
	my @fhs;
	my @gi2taxFiles;
	my $fh;
	my $gi;
	my $taxon;
	my $file;
	my $i;
	while (<$in>)
	{
		chomp;
		($gi, $taxon) = split(/\t/);
		$i = int( $gi / BLAST_PART_SIZE );
		unless ( $fh = $fhs[$i] )
		{
			$file = $gi2taxFiles[$i] = "$tmpDir/gi2tax.$i";
			$fhs[$i] = $fh = new IO::File(">$file") or die("Unable to write file, $file: $!");
		}
		print $fh $gi, "\t", $taxon, "\n";
	}
	close($in);
	foreach $fh (@fhs) { defined($fh) and $fh->close }

	# SPLIT BLAST FASTA FILE BY GI
	$tree->log("Splitting Blast $dbName FASTA in ", scalar(@gi2taxFiles), " parts");
	@fhs = ();
	my @fastaFiles;
	for ( $i=0; $i<=$#gi2taxFiles; ++$i )
	{
		$gi2taxFiles[$i] or next;
		$file = $fastaFiles[$i] = "$tmpDir/$dbName.$i";
		$fhs[$i] = new IO::File(">$file") or die("Unable to write $file: $!");
	}
	if ( $blastFastaFile =~ /\.gz$/ )
	{
		open($in, "gunzip -c $blastFastaFile|") or die("Unable to read gzipped file, $blastFastaFile: $!");
	} else
	{
		open($in, '<', $blastFastaFile) or die("Unable to read file, $blastFastaFile: $!");
	}
	my @numUnknown = ();
	while (<$in>)
	{
		if (/^>gi\|(\d+)\|/)
		{
			$i = int($1/BLAST_PART_SIZE);
			$fh = $fhs[$i];
		}
		if ( $fh ) { print $fh $_ } else { $numUnknown[$i]++ }
	}
	close($in);
	foreach $fh (@fhs) { defined($fh) and $fh->close }
	for ( $i=0; $i<=$#fastaFiles; $i++ )
	{
		$numUnknown[$i] and $tree->log("[Part $i] Skipped $numUnknown[$i] seqs of unknown taxa");
	}
	return (\@gi2taxFiles, \@fastaFiles);
}

# Load BLAST DB data into RefTree.  Requires db name (either 'nt' or 'nr').
sub readBlastFastaFile
{
	my ($tree, $dbName, $gi2taxFile, $blastFastaFile, $part) = @_;

	# VALIDATE INPUT
	defined($dbName) or die("DB name required (nt|nr)\n");
	defined($gi2taxFile) or die("GI:tax file required\n");
	defined($blastFastaFile) or die("Blast DB file required\n");
	defined($part) or die("Part number required\n");

	# LOAD GI2TAX HASH
	my %gi2tax;
	my $in;
	if ( -f $gi2taxFile )
	{
		open($in, '<', $gi2taxFile) or die("Unable to open GI:tax file, $gi2taxFile: $!");
	} elsif ( -f "$gi2taxFile.gz" )
	{
		open($in, "gunzip -c $gi2taxFile.gz |") or die("Unable to open gzipped GI:tax file, $gi2taxFile.gz: $!");
	} else
	{
		die("GI:tax file not found: $gi2taxFile");
	}
	while (my $line = <$in>)
	{
		chomp $line;
		my ($gi, $tax) = split(/\t/, $line);
		$gi2tax{$gi} = $tax;
	}
	close($in);

	# PROCESS FASTA FILE
	my $outFile; # currently open tmpFile
	my $out;
	my $numOkay = 0;
	my $numUnknown = 0;
	open($in, '<', $blastFastaFile) or die("Unable to open FASTA file, $blastFastaFile: $!");
	my $gi;
	my $accSrc;
	my $accId;
	my $taxon;
	my $node;
	while (<$in>)
	{
		if (/^>gi\|(\d+)\|(\S+)\|(\S+\.\d+)\|/)
		{
			($gi, $accSrc, $accId) = ($1,$2,$3);
			int($gi/BLAST_PART_SIZE) == $part or next;
			unless ( $taxon = $gi2tax{$gi} and $node = $tree->node($taxon) )
			{
				++$numUnknown;
				next;
			}
			++$numOkay;
			my $tmpFile = $node->dbTmpFile($dbName, $part);
			if ( $tmpFile ne $outFile )
			{
				$outFile = $tmpFile;
				defined($out) and close($out);
				open($out, '>>', $outFile) or die("Unable to append outFile, $outFile: $!");
			}
			print $out ">gi|$gi|$accSrc|$accId taxid=$taxon\n";
		}
		elsif ( defined($out) ) { print $out $_ }
	}
	close($in);
	defined($out) and close($out);
	$numUnknown and $tree->log("Excluded $numUnknown sequences of unknown taxa");
}

###########
## REFSEQ

sub importRefSeq
{
	my ($tree, $refseqDir) = @_;
	$refseqDir or die("Missing required refseq dir\n");
	$refseqDir = File::Spec->rel2abs($refseqDir);
	-d $refseqDir or die("Invalid refseq dir: $refseqDir\n");
	$refseqDir =~ /\/complete\/?$/ or die("Require the RefSeq complete/ folder; got: $refseqDir\n");

	# READ REFSEQ DIR
	defined($refseqDir) or die("RefSeq dir required\n");
	my %inFiles =
	(
		genomic => [],
		protein => [],
		nonredundant_protein => [],
		rna => []
	);
	opendir(DIR, $refseqDir) or die("Unable to open dir, $refseqDir: $!");
	my @inFiles = sort grep {/\.gz$/} readdir(DIR);
	closedir(DIR);
	foreach (@inFiles)
	{
		if (/complete\.(\d+)\.genomic\.gbff\.gz$/)
		{
			$inFiles{genomic}->[$1] = "$refseqDir/$_";
		}
		elsif (/complete\.nonredundant_protein\.(\d+)\.protein\.gpff\.gz$/)
		{
			$inFiles{nonredundant_protein}->[$1] = "$refseqDir/$_";
		}
		elsif (/complete\.(\d+)\.protein\.gpff\.gz$/)
		{
			$inFiles{protein}->[$1] = "$refseqDir/$_";
		}
		elsif (/complete\.(\d+)\.rna\.gbff\.gz$/)
		{
			$inFiles{rna}->[$1] = "$refseqDir/$_";
		}
	}
	my @dbs = sort keys %inFiles;

	# READ EACH MOLECULE'S FILES
	foreach my $dbName ( @dbs )
	{
		$tree->newDb($dbName, "$REFTREE_DIR/DATABASES/$dbName");
		my $tmpDir = $tree->mkDbTmpDirs($dbName);
		my $lastPart = $#{$inFiles{$dbName}};
		for ( my $part = 0; $part <= $lastPart; ++$part )
		{
			$pm->start and next;
			my $file = $inFiles{$dbName}->[$part];
			if ( $file )
			{
				$tree->log("Reading $dbName part $part of $lastPart");
				readRefSeqGenBankFile($tree, $part, $file, $dbName);
			}
			$pm->finish;
		}
		$pm->wait_all_children;

		# IMPORT DB
		$tree->aggregate($dbName);

		# CLEANUP
		system("rm -rf $tmpDir");
	}
}

# split a RefSeq file in Genbank format by appending to appropriate tmpFiles
sub readRefSeqGenBankFile
{
    my ($tree, $part, $inFile, $dbName) = @_;

	# INIT VARS
    my $counter = 0;
	my $counterUnk = 0;
	my $taxon; # a taxon has many records
	my $accession; # accession.version ID of record
    my $gi; # NCBI GenBank ID of record
	my %toDo; # genbank records without sequence
	my %features;
	my $outFile; # FASTA outfile path
	my $out; # FASTA outfile handle

	# READ GENBANK FILE
    open(my $in, "gunzip -c $inFile|") or die("Unable to open genbank file, $inFile: $!");
	while (<$in>)
    {
		if (/^VERSION\s+(\S+)\s+GI:(\d+)$/)
		{
			$accession = $1;
			$gi = $2;
		}
		elsif (/^\s+\/organelle="(.+)"$/) { $features{organelle} = $1 }
		elsif (/^\s+\/transl_table=(\d+)$/) { $features{gencode} = $1 }
		elsif (/^\s+\/plasmid="(.+)"$/) { $features{plasmid} = $1 }
		elsif (/^\s+\/db_xref="taxon:(\d+)"$/)
		{
			$taxon = $1;
			if ( my $node = $tree->node($taxon) )
			{
				# TAXON IS KNOWN; BEGIN APPENDING TMPFILE
				++$counter;
				my $tmpFile = $node->dbTmpFile($dbName, $part);
				if ( $tmpFile ne $outFile )
				{
					# CLOSE PREVIOUS OUTFILE AND SET OUTFILE BUT DO NOT OPEN YET
					if ( defined($out) )
					{
						close($out);
						$out = undef;
					}
					$outFile = $tmpFile;
				}
			} else
			{
				# TAXON IS UNKNOWN; SKIP TO END OF RECORD AND RESET VARS
				++$counterUnk;
				while (<$in>) { /^\/\// and last }
				$accession = $gi = $taxon = undef;
				%features = ();
			}
		}
		elsif (/^ORIGIN/)
		{
			# not all "genomic" have this tag (have "CONTIG" instead) and
			# the sequence must be retrieved from the FASTA file(s)

			# PRINT FASTA
			defined($out) or open($out, '>>', $outFile) or die("Unable to append outFile, $outFile: $!");
			my @features = ( "taxid=$taxon" );
			push @features, "$_=$features{$_}" foreach sort keys %features;
			print $out ">gi|$gi|ref|$accession ".join(',', @features)."\n";
			while (<$in>)
			{
				if (/^\s+\d+ (.+)$/)
				{
					my $seq = uc($1);
					$seq =~ s/\s//g;
					print $out $seq, "\n";
				} elsif (/^\/\//)
				{
					last;
				}
			}

			# INIT NEXT RECORD
			$accession = $gi = $taxon = undef;
			%features = ();
		}
		elsif (/^\/\//)
		{
			# END OF GENBANK RECORD, WITHOUT SEQUENCE
			$taxon or die("Missing taxon if $inFile gi|$gi\n");
			$accession or die("Missing accession in $inFile gi|$gi\n");
			$outFile or die("Missing outFile in $inFile gi|$gi\n");

			# ADD TO FASTA QUEUE AND INIT RECORD FOR NEXT
			$toDo{$gi} = [ $taxon, $accession, $outFile, \%features ];
			$accession = $gi = $taxon = undef;
			%features = ();
		}
	}
    close($in);
	defined($out) and close($out);
	#$tree->log("$counter records of known taxa read");

	# IF NECESSARY, FIND MATCHING FASTA FILE(S) AND EXTRACT SEQUENCES MISSING FROM REFSEQ FILE
	scalar(keys %toDo) and $tree->_readRefSeqFastaFile($inFile, \%toDo);

	# WARN ABOUT UNKNOWN TAXA
	$counterUnk and warn("Excluded $counterUnk unknown taxa in $dbName part $part\n");
	return $counter;
}

=item _readRefSeqFastaFile

When RefSeq GenBank file is missing some sequences, get them from the associated Fasta file(s).

=cut

sub _readRefSeqFastaFile
{
	my ($tree, $genbankFile, $toDo) = @_;
	my $numToDo = scalar(keys %$toDo) or return;
	#$tree->log("Looking in FASTA files for $numToDo sequences");

	# FIND ALL FASTA FILES MATCHING THIS REFSEQ GENBANK FILE
	my @inFastaFiles;
	if ( $genbankFile =~ /^(\/.+)\/complete\.(\d+)\.genomic\.gbff\.gz$/ )
	{
		my $inDir = $1;
		my $inputFileId = $2;
		opendir(DIR, $inDir) or die("Unable to read dir, $inDir: $!");
		@inFastaFiles = map { "$inDir/$_" } sort grep { /^complete\.$inputFileId\.[\d\.]+\.genomic\.fna\.gz$/ } readdir(DIR);
		closedir(DIR);
	} elsif ( $genbankFile =~ /^(\/.+)\/complete\.(\d+)\.protein\.gpff\.gz$/ )
	{
		my $inDir = $1;
		my $inputFileId = $2;
		opendir(DIR, $inDir) or die("Unable to read dir, $inDir: $!");
		@inFastaFiles = map { "$inDir/$_" } sort grep { /^complete\.$inputFileId\.protein\.faa\.gz$/ } readdir(DIR);
		closedir(DIR);
	} elsif ( $genbankFile =~ /^(\/.+)\/complete\.nonredundant_protein\.(\d+)\.protein\.gpff\.gz$/ )
	{
		my $inDir = $1;
		my $inputFileId = $2;
		opendir(DIR, $inDir) or die("Unable to read dir, $inDir: $!");
		@inFastaFiles = map { "$inDir/$_" } sort grep { /^complete\.nonredundant_protein\.$inputFileId\.protein\.faa\.gz$/ } readdir(DIR);
		closedir(DIR);
	} elsif ( $genbankFile =~ /^(\/.+)\/complete\.(\d+)\.rna\.gbff\.gz$/ )
	{
		my $inDir = $1;
		my $inputFileId = $2;
		opendir(DIR, $inDir) or die("Unable to read dir, $inDir: $!");
		@inFastaFiles = map { "$inDir/$_" } sort grep { /^complete\.$inputFileId\.rna\.fna\.gz$/ } readdir(DIR);
		closedir(DIR);
	} else
	{
		warn("Error parsing filename: $genbankFile; unable to retrieve $numToDo missing sequences\n");
		return;
	}
	unless (@inFastaFiles)
	{
		warn("Fasta file(s) matching $genbankFile not found; $numToDo seqs missing\n");
		return;
	}

	# READ FASTA FILES
	my $counter = 0;
	foreach my $inFastaFile (@inFastaFiles)
	{
		#$tree->log("Reading $inFastaFile");
		open(my $in, "gunzip -c $inFastaFile|") or die("Unable to read FASTA file, $inFastaFile: $!");
		my $out = undef;
		my $outFile = undef;
		while (<$in>)
		{
			if (/^>gi\|(\d+)/)
			{
				my $gi = $1;
				if ( exists($toDo->{$gi}) )
				{
					++$counter;
					my ($taxon, $accession, $tmpFile, $features) = @{$toDo->{$gi}};
					delete($toDo->{$gi});
					if ( $outFile ne $tmpFile )
					{
						defined($out) and close($out);
						open($out, '>>', $outFile = $tmpFile) or die("Unable to append outFile, $outFile: $!");
					}
					my @features = ( "taxid=$taxon" );
					push @features, "$_=".$features->{$_} foreach sort keys %$features;
					print $out ">gi|$gi|ref|$accession ".join(',', @features)."\n";
				} else
				{
					defined($out) and close($out);
					$outFile = $out = undef;
				}
			} elsif ( defined($out) )
			{
				print $out $_;
			}
		}
		close($in);
		defined($out) and close($out);
	}
	#$tree->log("$counter sequences retrieved");

	# PRINT WARNING IF ANY NOT FOUND
	$numToDo = scalar(keys %$toDo) and warn("\t$numToDo sequences were not found\n");
}


=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2015 by the United States Department of Energy Joint Genome Institute

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

1;

__END__

# DEPRECATED:

=item _importAttributes

Private method imports an attribute from tmp files.

=cut

sub _importRefSeqAttribute
{
	my ($tree, $tmpDir, $attribute) = @_;
	my $tmpFile = "$tmpDir/$attribute.txt";
	open(my $out, '>', $tmpFile) or die($!);
	opendir(DIR, $tmpDir) or die($!);
	my @fileIds = grep { /^\d+$/ } readdir(DIR);
	closedir(DIR);
	foreach my $fileId (@fileIds)
	{
		my $dir = "$tmpDir/$fileId";
		opendir(DIR, $dir) or die($!);
		my @files = grep { /\.$attribute$/ } readdir(DIR);
		closedir(DIR);
		foreach my $file (@files)
		{
			open(my $in, '<', "$dir/$file") or die($!);
			while (<$in>) { print $out $_ }
			close($in);
			unlink("$dir/$file");
		}
	}
	$tree->importAttribute($attribute, "$REFTREE_DIR/ATTRIBUTES", $tmpFile);
	unlink($tmpFile);
}

