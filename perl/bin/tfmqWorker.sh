#!/bin/bash -l

if [ -z "$1" ]; then
	echo "Task name required" >&2
	exit 1
fi
module load tfmq
exec tfmq-worker -q "$1"
