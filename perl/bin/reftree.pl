#!/usr/bin/env perl

=pod

=head1 NAME

reftree.pl

=head1 DESCRIPTION

This executable provides command-line access to some common RefTree functions.

=head1 DATA SOURCES

The default available NCBI databases are listed below.  In addition, user-supplied databases are supported.

=over 5

=item genomic

RefSeq "complete" genomic.

=item protein

RefSeq "complete" protein

=item nonredundant_protein

RefSeq "complete" nonredundant_protein

=item rna

RefSeq "complete" rna

=item nt

Blast nonredundant nucleotide database.

=item nr

Blast nonredundant protein database.

=back

=head1 ATTRIBUTES

The default included attributes are listed below.  In addition, user-supplied attributes are supported.

=over 5

=item name

Scientific name

=item code

Genetic code (translation table) used by the taxon.  See: NCBI documentation for meaning of the numeric codes.

=item rankName

Taxonomic rank name (e.g. 'genus').

=item rankId

Taxonomic rank as an integer, which allows use of comparison operators.

=over 10

=item	no rank => undef,

=item	superkingdom => 0,

=item	kingdom => 1,

=item	subkingdom => 2,

=item	superphylum => 3,

=item	phylum => 4,

=item	subphylum => 5,

=item	superclass => 6,

=item	class => 7,

=item	subclass => 8,

=item	infraclass => 9,

=item	superorder => 10,

=item	order => 11,

=item	suborder => 12,

=item	infraorder => 13,

=item	parvorder => 14,

=item	superfamily => 15,

=item	family => 16,

=item	subfamily => 17,

=item	tribe => 18,

=item	subtribe => 19,

=item	genus => 20,

=item	subgenus => 21,

=item	species group => 22,

=item	species subgroup => 23,

=item	species => 24,

=item	subspecies => 25,

=item	varietas => 26,

=item	forma => 27

=back

=back

=head1 OPTIONS

=head2 Selection Criteria

Specify which nodes or subtrees you wish to select using exactly one of the following options:

=over 5

=item --node C<taxonId>, ...

Output record for each node in list or use '-' to read from standard input.

=item --subtree C<taxonId>, ...

Output records for all nodes in each subtree in list or use '-' to read from standard input.

=item --select <string>

Act only upon nodes for which the statement evaluates to TRUE.  Refer to attributes as variables (i.e. preceded by C<$>; e.g. C<$code>, the name of the genetic code attribute), Perl comparison operators (numeric comparisons: ==, !=, <, >, >=, <=; string comparisons: eq, ne, gt, lt, ge, le), the C<defined> function, and logic statements (and, or, not, xor).  Note: enclose the entire statement in quotes so it's treated as a string.  Works with C<--subtree> and C<--foreach>.  Be sure to enclose the entire statement in single-quotes.  Examples:

=over 10

=item *

'$code == 11'

=item *

'!defined($organelle)'

=item *

'$code == 11 and !defined($organelle)'

=item *

'$rankId >= 24'

=item *

'$rankName eq "species"'

=back

=back

=head2 User-Specified Data

If accessing any user-specified databases or attributes, provide the folders which contain them.  All dbs and attributes under the listed folders become available (no need to specify each attribute separately).

=over 5

=item --dbDir C<path>, ...

Recursively search each folder in list to find available (user) DBs.

=item --attrDir C<path>, ...

Recursively search each folder in list to find avaiable (user) attributes.

=back

=head2 Output Options

=over 5

=item --db C<name>

Output records for this db; only one db may be queried at a time.  Optional.

=item --attributes C<field>, ...

List of taxon-specific fields to include in output: rankId, rankName, taxonomy, code, name, or user-supplied attribute(s).  The "taxonomy" option outputs the complete taxonomic path in the format: C<taxonId0:rankId0/...> (note that not all nodes have rankIds defined).

=item --outFile C<path>

Write output to file.  Optional; by default use standard output.

=back

=head2 Job Management

If you're analyzing large sets of nodes, RefTree can manage the jobs for you.  Create an executable file to process a single node or subtree, which reads from standard input and generates a single output file, with the path received as a parameter.  RefTree will automatically collect the results and prepares it as a RefTree database for subsequent retrieval via RefTree.

=over 5

=item --foreach C<taxon> C<outDir> -- C<exe> C<args>, ...

Perform an action on each node in the subtree starting at the specified taxon.  The results will be written to outDir, where the folder name shall be the db name.  Whatever follows the double-dash is used as the executable and arguments.  An instance is run for each node or subtree selected.  Use the special strings C<__TAXON__> and C<__OUTFILE__>, which are replaced with the appropriate taxon ID and output file, respectively.  It's the executable's responsibility to retrieve the records of the taxon from RefTree.

=item --resources C<string>

To execute the tasks on the computer cluster, use this option to provide the qsub parameters (e.g. "-l ram.c=xG, h_rt=xx:xx:xx"); tasks shall be executed using B<TaskFarmer> if installed, otherwise a batch job shall be used.  Without this parameter, the jobs are executed locally.  This script will block until all tasks are complete.

=item --slots C<int>

The number of processes to use when running locally or on the cluster with TaskFarmer, but is not applicable to batch jobs.

=item --keep

Use this option in order to keep temporary outfiles (purged by default); only applies to data that is aggregated (i.e. __OUTFILE__ option used).

=back

=head2 Importing User-Supplied Data

User data may be imported as a database or an attribute.  Generally, attributes are small data points and databases contain large, possibly multiple values per taxon.  Attributes may be used to direct traversals as they are stored in RAM.  While it's possible to use database records to direct traversals by using the Perl API, this script does not offer such functionality.

=over 5

=item --importAttributes C<name> C<infile> C<outDir>

Import attributes in tabular format, where the taxon ID is the first column, followed by the datum.

=item --importDb C<name> C<indir> C<outDir>

Import data in either FASTA or tabular format.  The former requires header lines which include the string "tax|<taxon>" or "tax=<taxon>"; while the later must be tab-delimited with the taxon ID in the first column.

=back

=head1 SEE ALSO

TaskFarmer by Seung-Jin Sul (unpublished and not bundled with RefTree).

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2015 by the United States Department of Energy Joint Genome Institute

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut


use strict;
use Getopt::Long;
use Pod::Usage;
use Env qw(REFTREE_DIR TMPDIR);
use RefTree;
use File::Path qw(make_path remove_tree);
use constant
{
	FALSE => 0, TRUE => 1, # for readability
	DEFAULT_NUM_PROCESSES => 32 # for local jobs
};
use Parallel::ForkManager;
use File::Basename;
use File::Spec;
use FindBin qw($Bin);
use DBI;
use File::Which qw(which);

# GLOBAL VARS
our $tfmq = which('tfmq-worker') ? TRUE : FALSE;

# OPTIONS
my @nodes;
my @subtrees;
my $select;
my @dbDir;
my @attrDir;
my $dbName;
my @fields;
my $outFile;
my @foreach;
my @resources;
my $slots;
my $keep;
my @importAttributes;
my @importDb;
my $taxdumpDir;
my $blastDir;
my $refseqDir;
my $help;
my $man;
GetOptions(
	# NODE SELECTION
	'nodes=i' => \@nodes,
	'subtrees=i' => \@subtrees,
	'select=s' => \$select,
	# USER DATA
	'dbDir=s' => \@dbDir,
	'attrDir=s' => \@attrDir,
	# OUTPUT OPTIONS
	'db=s' => \$dbName,
	'attributes=s' => \@fields,
	'outFile=s' => \$outFile,
	# JOB EXECUTION
	'foreach=s{2}' => \@foreach,
	'resources=s' => \@resources,
	'slots=i' => \$slots,
	'keep' => \$keep,
	# IMPORT USER DATA
	'importAttributes=s{3}' => \@importAttributes,
	'importDb=s{3}' => \@importDb,
	# DOCUMENTATION
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
if ( @fields == 1 and $fields[0] =~ /,/ ) { @fields = split(/,/, $fields[0]) }
$outFile and $outFile = File::Spec->rel2abs($outFile);

# USER DATABASES
@dbDir and RefTree::Databases->findDbs(@dbDir);
@attrDir and RefTree::Attributes->findAttributes(@attrDir);
if ( @importAttributes )
{
	RefTree->loadTree->importAttributes(@importAttributes);
	exit;
}
if ( @importDb )
{
	die("NYI\n");
	RefTree->loadTree->importDb(@importDb);
	exit;
}

# OUTPUT
my $fh = $outFile ? new IO::File(">$outFile") : undef;

# SELECTION
if ( @nodes )
{
	while ( my $taxon = nextRec(\@nodes) )
	{
		my $node = RefTree->loadNode($taxon) or die("Unknown taxon $taxon\n");
		$node->output($fh, \@fields, $dbName);
	}
}
elsif ( @subtrees )
{
	defined($select) and $select = prepareSelect($select);
	my $tree = RefTree->loadTree;
	while ( my $taxon = nextRec(\@subtrees) )
	{
		my $node = $tree->node($taxon) or die("Unknown taxon $taxon\n");
		$node->outputSubtree($select, $fh, \@fields, $dbName);
	}
}
elsif ( @foreach )
{
	@foreach == 2 or die("--foreach requires dbName and outputDir\n");
	my ($taxon, $outDir) = @foreach;
	@ARGV or die("--foreach requires the command to execute\n");
	$outFile and die("--outFile is not compatible with --foreach\n");
	@subtrees and die("--subtree is not compatible with --foreach\n");
	@nodes and die("--node is not compatible with --foreach\n");
	$outFile and die("--outFile is not compatible with --foreach\n");
	defined($select) and $select = prepareSelect($select);
	run($taxon, $select, \@ARGV, $outDir, $dbName, "@resources", $slots, $keep);
}
else { die("A valid command is required; see: --help or --man.\n") }
exit;

sub nextRec
{
	my $ar = shift;
	if ( $ar->[0] eq '-' )
	{
		my $line = <STDIN>;
		chomp $line;
		my ($taxon) = split(/\t/, $line);
		return $taxon;
	} else
	{
		return shift @$ar;
	}
}

# turns a string into a subroutine
sub prepareSelect
{
	my $stmt = shift;
	$stmt =~ /\$/ or die("Your select statement doesn't include any variables; be sure to enclose the entire statement in single-quotes or escape variables names so they're not interpolated by your shell.\n");
	$stmt or return undef;
	$stmt =~ /[;|`]/ and die("Invalid statement: $stmt\n");
	$stmt =~ s/([!()])/ $1 /g;
	$stmt =~ s/\s+/ /g;
	my @stmt = split(/\s+/);
	foreach (@stmt)
	{
		/^\$\w+$/ and next;
		/^?defined\(\$\w+\)$/ and next;
		/^=$/ and die("Statement contains assignment operator: $stmt\n");
		/^[lt|gt|le|ge|eq|ne|==|!=|<|>|<=|>=|!|and|or|xor|(|)]$/ and next;
		/^\d+$/ and next;
		/^(['"])[\w\s]$1$/ and next;
		die("Invalid statement: $stmt; contains: $_\n");
	}
	$stmt =~ s/\$(\w+)/\$node->get\('$1'\)/g;
	$stmt = "return $stmt ? 1 : 0";
	my $sub = sub
	{
		my $node = shift;
		return eval $stmt;
	};
	return $sub;
}

# Run an executable on each node of the subtree specified by the taxon ID, which has records in the specified database.  The executable is responsible for retrieving the data (via reftree.pl --node or the Perl RefTree API).  If the optional qsub parameter string is provided and TaskFarmer is installed, the tasks shall be run on the compute cluster, otherwise they are run locally, in which case the number of workers should be limited by the number of available cores.  The command may include the special strings __TAXON__ and __OUTFILE__, which shall be substituted with the taxon ID and output file path, respectively.  If the data is to be loaded into RefTree, it should be written to the specified output location; FASTA file headers must contain "tax|<taxonId> or "tax=<taxonId>"; tabular files must have the taxon ID in the first column.

sub run
{
	my ($taxon, $select, $cmd, $outDir, $dbName, $resources, $numWorkers, $keep) = @_;

	# VALIDATE
	$dbName or die("DB required");
	$outDir and $outDir = File::Spec->rel2abs($outDir);

	# VALIDATE COMMAND
	unless ( defined($cmd) and @$cmd > 0 ) { die("Command required") }
	my $hasOutfile = FALSE;
	my $hasTaxon = FALSE;
	foreach (@$cmd)
	{
		/__OUTFILE__/i and $hasOutfile = TRUE;
		/__TAXON__/i and $hasTaxon = TRUE;
	}
	unless ( $hasTaxon  )
	{
		die("Command missing __TAXON__ string; it's the responsbility of the executable to retrieve records from RefTree (e.g. reftree.pl --node __TAXON__).");
	}
	
	# VALIDATE OUTDIR
	$outDir or die("Output folder required\n");
	-d $outDir and die("Output dir already exists: $outDir\n");
	my ($taskName) = fileparse($outDir); # dbName of result

	# INIT TREE, VALIDATE NODE
	my $tree = RefTree->loadTree;
	my $node = $tree->node($taxon) or die("Unknown taxon $taxon\n");
	print "Foreach starting at ".$node->id." with input DB $dbName, output to $outDir, cmd=@$cmd\n";

	# MAKE TMPDIR -- USED BY JOB MANAGER AND FOR TASK OUTPUT IF IMPORTING RESULTS
	$node->newDb($taskName, $outDir);
	my $tmpDir = $node->dbTmpDir($taskName);
	-d $tmpDir and system("rm -rf $tmpDir");
	make_path($tmpDir) or die("Unable to mkdir, $tmpDir: $!");
	my %tmpDirs;
	$node->descend({
		preorder => sub
		{
			my ($node, $hr, $tmpDir) = @_;
			my $fileId = $node->get('file');
			exists ($hr->{$fileId}) and return 0;
			my $dir = "$tmpDir/$fileId";
			mkdir $dir or die("Unable to mkdir tmpdir: $dir: $!\n");
			$hr->{$fileId} = undef;
			return 1;
		},
		result => 'sum'
	}, \%tmpDirs, $tmpDir);

	# RUN JOB MANAGER -- BLOCKS UNTIL COMPLETE
	my $n;
	if ( ! $resources )
	{
		$n = _forkManager($node, $select, $tmpDir, $dbName, $numWorkers, $cmd);
	}
	else
	{
		if ( $tfmq and $hasOutfile)
		{
			$n = _taskFarmer($node, $select, $taskName, $tmpDir, $dbName, $numWorkers, $cmd, $resources);
		}
		else
		{
			$tfmq and warn("Command missing __OUTFILE__ so cannot use TaskFarmer; using batch job instead\n");
			$n = _batchJob($node, $select, $taskName, $tmpDir, $dbName, $cmd, $resources);
		}
	}
	print "$n tasks complete\n";

	# OPTIONALLY IMPORT RESULTS
	if ( $hasOutfile )
	{
		$node->aggregate($taskName, $keep);
		$keep or system("rm -rf $tmpDir");
	}
}

sub _forkManager
{
	my ($tree, $select, $tmpDir, $dbName, $numWorkers, $cmd) = @_;
	$numWorkers or $numWorkers = DEFAULT_NUM_PROCESSES;
	print "Begin ForkManager with $numWorkers processes; db=$dbName; tmpDir=$tmpDir; cmd=@$cmd\n";
	my $pm = new Parallel::ForkManager($numWorkers);
	my @fields = qw(taxonomy); # TODO MAKE THIS A PARAMETER
	my $numTasks = $tree->descend(
	{
		postorder => sub
		{
			my ($node, $dbName, $pm, $tmpDir, $cmd, $fieldsAR) = @_;
			my $offset = $node->get($dbName);
			defined($offset) or return 0;
			my $taxon = $node->id;
			my $fileId = $node->get('file');
			my $tmpFile = "$tmpDir/$fileId/$taxon.1";
			my @cmd = map { s/__TAXON__/$taxon/gir } map { s/__OUTFILE__/$tmpFile/gir } @$cmd;
			$pm->start and return 1;
			system(@cmd) == 0 or die("Error running command, @cmd: $!\n");
			$pm->finish;
		},
		select => $select,
		result => 'sum'
	}, $dbName, $pm, $tmpDir, $cmd, \@fields);
	$numTasks or die("There are no nodes which match the search criteria; nothing to do.");
	$pm->wait_all_children;
	return $numTasks;
}

sub _taskFarmer
{
	my ($tree, $select, $taskName, $tmpDir, $dbName, $numWorkers, $cmd, $resources) = @_;

	# CREATE TASK LIST
	my $taskList = "$tmpDir/tasks.txt";
	open(my $fh, '>', $taskList) or die("Unable to write file, $taskList: $!");
	my $numTasks = $tree->descend(
	{
		postorder => sub
		{
			my ($node, $dbName, $fh, $tmpDir, $cmd) = @_;
			defined($node->get($dbName)) or return 0;
			my $taxon = $node->id;
			my $fileId = $node->get('file');
			my $tmpFile = "$tmpDir/$fileId/$taxon.1";
			my @cmd = map { s/__TAXON__/$taxon/gir } map { s/__OUTFILE__/$tmpFile/gir } @$cmd;
			print $fh "@cmd:$tmpFile:0\n";
			return 1;
		},
		select => $select,
		result => 'sum'
	}, $dbName, $fh, $tmpDir, $cmd);
	close($fh);
	$numTasks or die("There are no nodes which match the search criteria; nothing to do.");

	# START TASK FARMER
	$numWorkers or $numWorkers = int($numTasks/5);
	system("qsub $resources -N $taskName -t 1:$numWorkers -b y -w e -cwd -v TMPDIR=$tmpDir $Bin/tfmqWorker.sh $taskName") == 0 or die("Unable to request $numWorkers workers via qsub: $!");
	system("module load tfmq; tfmq-client -i $taskList -q $taskName") == 0 or die("Unable to start tfmq queue $taskName, file $taskList: $!");
	return $numTasks;
}

sub _batchJob
{
	my ($tree, $select, $taskName, $tmpDir, $dbName, $cmd, $resources) = @_;

	# CREATE TASK LIST
	my $taskList = "$tmpDir/taxa.txt";
	open(my $fh, '>', $taskList) or die("Unable to write file, $taskList: $!");
	my $numTasks = $tree->descend(
	{
		postorder => sub
		{
			my ($node, $dbName, $fh, $tmpDir, $cmd) = @_;
			defined($node->get($dbName)) or return 0;
			my $taxon = $node->id;
			my $fileId = $node->get('file');
			my $tmpFile = "$tmpDir/$fileId/$taxon.1";
			my @cmd = map { s/__TAXON__/$taxon/gir } map { s/__OUTFILE__/$tmpFile/gir } @$cmd;
			print $fh "@cmd\n";
			return 1;
		},
		select => $select,
		result => 'sum'
	}, $dbName, $fh, $tmpDir, $cmd);
	close($fh);
	$numTasks or die("There are no nodes which match the search criteria; nothing to do.");

	# QUEUE BATCH JOB
	system("qsub -b y -w e $resources -cwd -N $taskName -t 1:$numTasks -sync y -v TMPDIR=$tmpDir $Bin/batchWorker.sh $taskList") == 0 or die("Batch job error\n");
	return $numTasks;
}

__END__
