#!/bin/bash -l

$REFTREE_ENV
PARAMS=''
TASK="$SGE_TASK_ID"
FILE="$1"
shift
COMMAND=`head -"$TASK" "$FILE" | tail -1`
exec $COMMAND
